package product.dg.xshipper.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.model.GigTicket;

public class GroupGigTicket {

    enum TYPE {
        SAME_START_END_DISTRICTS,
        SAME_START_DISTRICTS,
        SAME_END_DISTRICTS,
        SAME_START_END_WARD
    }

    public String mKey;
    public TYPE mType;
    List<GigTicket> mData = new ArrayList<>();

    public GroupGigTicket(TYPE type) {
        this.mType = type;
    }

    public List<GigTicket> getData() {
        return mData;
    }

    public void addData(GigTicket data) {
        this.mData.add(data);
    }
}
