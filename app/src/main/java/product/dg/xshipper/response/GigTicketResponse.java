package product.dg.xshipper.response;

import com.google.android.gms.maps.model.GroundOverlay;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketGroup;
import product.dg.xshipper.model.PresentGigTicket;

public class GigTicketResponse {
    @SerializedName("data")
    List<GigTicket> mData;

    List<PresentGigTicket> mPresentData = new ArrayList<>();


    public GigTicketResponse() {}

    public List<PresentGigTicket> getPresentData() {
        analytics();
        return mPresentData;
    }

    public List<GigTicket> getData() {
        return mData;
    }

    public List<GroupGigTicket> getGroupTicket(GroupGigTicket.TYPE type) {
        switch (type) {
            case SAME_START_END_DISTRICTS:
                return mSameStartEndDistricts;
            case SAME_START_DISTRICTS:
                return mSameStartDistricts;
            case SAME_END_DISTRICTS:
                return mSameEndDistricts;
            case SAME_START_END_WARD:
                return mSameStartEndWards;
            default:
                break;
        }

        return null;
    }

    List<GroupGigTicket> mSameStartEndDistricts;
    List<GroupGigTicket> mSameStartDistricts;
    List<GroupGigTicket> mSameEndDistricts;
    List<GroupGigTicket> mSameStartEndWards;
    public void analytics() {
        mSameStartEndDistricts = new ArrayList<>();
        mSameStartDistricts = new ArrayList<>();
        mSameEndDistricts = new ArrayList<>();
        mSameStartEndWards = new ArrayList<>();

        for (GigTicket gt : mData) {
            try {

                GigTicket _gt = (GigTicket) gt;
                String toWard = _gt.getToWard();
                String toDistrict = _gt.getToDistrict();

                String from = _gt.getFrom();
                String[] fromSplit = from.split(",");
                String fromDistricts = fromSplit[fromSplit.length - 3].trim();
                String fromWard = fromSplit[fromSplit.length - 4].trim();

                String p_key = fromWard + "_" + toWard;
                boolean isGood = getMatchedGroupAndAddIfExisted(p_key, GroupGigTicket.TYPE.SAME_START_END_WARD, _gt);

                if (isGood == false) {
                    p_key = fromDistricts + "_" + toDistrict;
                    isGood = getMatchedGroupAndAddIfExisted(p_key, GroupGigTicket.TYPE.SAME_START_END_DISTRICTS, _gt);
                }

                if (isGood == false) {
                    p_key = fromDistricts;
                    isGood = getMatchedGroupAndAddIfExisted(p_key, GroupGigTicket.TYPE.SAME_START_DISTRICTS, _gt);
                }

                if (isGood == false) {
                    getMatchedGroupAndAddIfExisted(p_key, GroupGigTicket.TYPE.SAME_END_DISTRICTS, _gt);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        mPresentData.clear();
        List<GigTicket> temp = new ArrayList<>();

        for (GroupGigTicket ggt : mSameStartEndDistricts) {
            if (ggt.getData().size() > 1) {
                PresentGigTicket pgt = new PresentGigTicket();
                pgt.setIsGroup(true);

                GigTicketGroup gtg = new GigTicketGroup();
                gtg.setData(ggt.getData());
                gtg.setTitle("Gói đơn hàng chung Quận/Huyện");

                GigTicket gt = ggt.getData().get(0);
                String toDistrict = gt.getToDistrict();
                String toProvince = gt.getToProvince();

                String from = gt.getFrom();
                String[] fromSplit = from.split(",");
                String fromProvince = fromSplit[fromSplit.length - 2].trim();
                String fromDistricts = fromSplit[fromSplit.length - 3].trim();

                gtg.setFrom(fromDistricts + ", " + fromProvince);
                gtg.setTo(toDistrict + ", " + toProvince);
                gtg.setSize(String.valueOf(ggt.getData().size()));
                pgt.setGigTicketGroup(gtg);
                mPresentData.add(pgt);

                for (GigTicket t : ggt.getData()) {
                    if (mData.contains(t)) {
                        mData.remove(t);
                    }
                }
            }
        }

        for (GroupGigTicket ggt : mSameStartDistricts) {
            if (ggt.getData().size() > 1) {
                PresentGigTicket pgt = new PresentGigTicket();
                pgt.setIsGroup(true);

                GigTicketGroup gtg = new GigTicketGroup();
                gtg.setData(ggt.getData());
                gtg.setTitle("Gói đơn hàng chung Quận/Huyện (Điểm Lấy)");

                GigTicket gt = ggt.getData().get(0);
                String from = gt.getFrom();
                String[] fromSplit = from.split(",");
                String fromProvince = fromSplit[fromSplit.length - 2].trim();
                String fromDistricts = fromSplit[fromSplit.length - 3].trim();

                gtg.setFrom(fromDistricts + ", " + fromProvince);
                gtg.setTo("--------------------");
                gtg.setSize(String.valueOf(ggt.getData().size()));
                pgt.setGigTicketGroup(gtg);
                mPresentData.add(pgt);

                for (GigTicket t : ggt.getData()) {
                    if (mData.contains(t)) {
                        mData.remove(t);
                    }
                }
            }
        }

        for (GroupGigTicket ggt : mSameEndDistricts) {
            if (ggt.getData().size() > 1) {
                PresentGigTicket pgt = new PresentGigTicket();
                pgt.setIsGroup(true);

                GigTicketGroup gtg = new GigTicketGroup();
                gtg.setData(ggt.getData());
                gtg.setTitle("Gói đơn hàng chung Quận/Huyện (Điểm Nhận)");

                GigTicket gt = ggt.getData().get(0);
                String toDistrict = gt.getToDistrict();
                String toProvince = gt.getToProvince();

                gtg.setFrom("--------------------");
                gtg.setTo(toDistrict + ", " + toProvince);
                gtg.setSize(String.valueOf(ggt.getData().size()));
                pgt.setGigTicketGroup(gtg);
                mPresentData.add(pgt);

                for (GigTicket t : ggt.getData()) {
                    if (mData.contains(t)) {
                        mData.remove(t);
                    }
                }
            }
        }

        for (GroupGigTicket ggt : mSameStartEndWards) {
            if (ggt.getData().size() > 1) {
                PresentGigTicket pgt = new PresentGigTicket();
                pgt.setIsGroup(true);

                GigTicketGroup gtg = new GigTicketGroup();
                gtg.setData(ggt.getData());
                gtg.setTitle("Gói đơn hàng chung Phường/Xã");

                GigTicket gt = ggt.getData().get(0);
                String toDistrict = gt.getToDistrict();
                String toProvince = gt.getToProvince();
                String toWard = gt.getToWard();

                String from = gt.getFrom();
                String[] fromSplit = from.split(",");
                String fromProvince = fromSplit[fromSplit.length - 2].trim();
                String fromDistricts = fromSplit[fromSplit.length - 3].trim();
                String fromWard = fromSplit[fromSplit.length - 4].trim();

                gtg.setFrom(fromWard + ", " + fromDistricts + ", " + fromProvince);
                gtg.setTo(toWard + ", " + toDistrict + ", " + toProvince);
                gtg.setSize(String.valueOf(ggt.getData().size()));
                pgt.setGigTicketGroup(gtg);
                mPresentData.add(pgt);

                for (GigTicket t : ggt.getData()) {
                    if (mData.contains(t)) {
                        mData.remove(t);
                    }
                }
            }
        }

        for (GigTicket ggt : mData) {
            PresentGigTicket t = new PresentGigTicket();
            t.setIsGroup(false);
            t.setGigTicket(ggt);
            mPresentData.add(t);
        }
    }

    private boolean getMatchedGroupAndAddIfExisted(String pkey, GroupGigTicket.TYPE type, GigTicket gt) {
        List<GroupGigTicket> source;
        if (type == GroupGigTicket.TYPE.SAME_START_END_DISTRICTS) {
            source = mSameStartEndDistricts;
        }
        else if (type == GroupGigTicket.TYPE.SAME_START_DISTRICTS) {
            source = mSameStartDistricts;
        }
        else if (type == GroupGigTicket.TYPE.SAME_END_DISTRICTS) {
            source = mSameEndDistricts;
        }
        else {
            source = mSameStartEndWards;
        }
        for (GroupGigTicket ggt : source) {
            if (ggt.mKey.equalsIgnoreCase(pkey)) {
                ggt.addData(gt);
                return true;
            }
        }

        GroupGigTicket ggt = new GroupGigTicket(type);
        ggt.mKey = pkey;
        ggt.addData(gt);
        source.add(ggt);
        return false;
    }
}
