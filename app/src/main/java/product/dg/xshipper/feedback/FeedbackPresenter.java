package product.dg.xshipper.feedback;

import java.lang.ref.WeakReference;

import product.dg.xshipper.R;


public class FeedbackPresenter implements FeedbackInterfaces.ProvidedPresenterOps, FeedbackInterfaces.RequiredPresenterOps{
    private WeakReference<FeedbackInterfaces.RequiredViewOps> mView;
    private FeedbackInterfaces.ProvidedModelOps mModel;

    FeedbackPresenter(FeedbackInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(FeedbackInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(FeedbackInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void send(String content) {
        if (content.trim().equalsIgnoreCase("")) {
            showDialog(R.string.dialog_title_error, R.string.feedback_content_empty);
            return;
        }
        mView.get().showWaiting();
        mModel.send(content);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void sendSuccess() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().sendSuccess();
            }
        });
    }
}
