package product.dg.xshipper.feedback;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import product.dg.xshipper.R;


public class FeedbackFragment extends Fragment implements FeedbackInterfaces.RequiredViewOps {

    EditText mContent;
    Button mSubmit;
    private FeedbackInterfaces.ProvidedPresenterOps mPresenter;
    private ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_feedback, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        mContent = (EditText) view.findViewById(R.id.edt_content);
        mSubmit = (Button) view.findViewById(R.id.btn_submit);
    }

    void setupControlEvents() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.send(mContent.getText().toString());
            }
        });
    }

    void init() {
        FeedbackPresenter presenter = new FeedbackPresenter(this);
        FeedbackModel model = new FeedbackModel(presenter);

        presenter.setModel(model);
        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) FeedbackFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
            }
        });
    }

    @Override
    public void hideWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void sendSuccess() {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(R.string.app_name))
                .setMessage(getString(R.string.feedback_success))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mContent.setText("");
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
