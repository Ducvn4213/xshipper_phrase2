package product.dg.xshipper.service;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.WindowManager;
import android.widget.OverScroller;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import product.dg.xshipper.MainActivity;
import product.dg.xshipper.R;
import product.dg.xshipper.util.FS;
import product.dg.xshipper.util.NoLocationServiceActivity;

public class SendPosService extends Service {

    public static final int RESULT_CODE = 101;

    private String TAB = "GPSService";
    private LocationManager locationManager;
    private Context context;
    private FSService mService = FSService.getInstance();

    private double longitude = 0;
    private double latitude = 0;

    public SendPosService() {
        Log.e(TAB, "GPSService");
    }

    ScheduledExecutorService scheduler;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAB, "onCreate");
        scheduler = Executors.newSingleThreadScheduledExecutor();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        context = getApplicationContext();
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAB, "onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return super.onStartCommand(intent, flags, startId);
            }

            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 5000, 0,
                    networkLocationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    3000, 0, gpsLocationListener);



            scheduler.scheduleAtFixedRate
                    (new Runnable() {
                        public void run() {
                            boolean checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                            boolean checkNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                            if (!checkGPS && !checkNetwork) {
                                try {
                                    if (MainActivity.getInstance() != null && !FSService.isPerformNoLocationActivity) {
                                        Intent intent = new Intent(SendPosService.this, NoLocationServiceActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                            else {
                                mService.sendPosition(latitude, longitude);
                            }
                        }
                    }, 0, 5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAB, "onDestroy");
        scheduler.shutdown();
    }

    private final LocationListener gpsLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(networkLocationListener);
            double _longitude = location.getLongitude();
            double _latitude = location.getLatitude();
            longitude = _longitude;
            latitude = _latitude;

            mService.lastLatitude = latitude;
            mService.lastLongitude = longitude;
        }
    };

    private final LocationListener networkLocationListener = new LocationListener(){
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras){}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onLocationChanged(Location location) {
            double _longitude = location.getLongitude();
            double _latitude = location.getLatitude();
            longitude = _longitude;
            latitude = _latitude;

            mService.lastLatitude = latitude;
            mService.lastLongitude = longitude;
        }
    };

}

