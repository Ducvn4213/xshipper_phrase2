package product.dg.xshipper.service;

import android.content.Context;

import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import product.dg.xshipper.adapter.model.NewsList;
import product.dg.xshipper.adapter.model.Statistic;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.Direction;
import product.dg.xshipper.model.DirectionLocation;
import product.dg.xshipper.model.Distance;
import product.dg.xshipper.model.SnappedResponse;
import product.dg.xshipper.model.User;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.network.Network;
import product.dg.xshipper.service.network.Param;
import product.dg.xshipper.service.network.Response;
import product.dg.xshipper.util.FS;
import product.dg.xshipper.util.Utils;

public class FSService {
    public interface Callback<E> {
        public void onSuccess(E data);
        public void onFail(String error);
    }

    public double lastLatitude;
    public double lastLongitude;

    private User mCurrentUser;
    private User mCurrentFacebookUser;

    private int mSubIndex = 0;
    private String mCurrentRegistrationID = "";

    Network network = Network.getInstance();

    private static FSService instance;
    private FSService() {
    }

    public static FSService getInstance() {
        if (instance == null) {
            instance = new FSService();
        }

        return instance;
    }

    public static Boolean isPerformNoLocationActivity = false;

    public void setSubIndex(int index) {
        mSubIndex = index;
    }

    public int getSubIndex() {
        return mSubIndex;
    }

    public void saveUserCredential(Context context, String username, String password) {
        Utils.saveValue(context, FS.SP_USERNAME_KEY, username);
        Utils.saveValue(context, FS.SP_PASSWORD_KEY, password);
    }

    public boolean isLogin() {
        return mCurrentUser != null || mCurrentFacebookUser != null;
    }

    public void logout() {
        this.mCurrentUser = null;
        this.mCurrentFacebookUser = null;

        LoginManager.getInstance().logOut();
    }

    public User getCurrentActiveUser() {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            return null;
        }

        return currentUser;
    }

    public String getUsernameCredential(Context context) {
        return Utils.getValue(context, FS.SP_USERNAME_KEY);
    }

    public String getPasswordCredential(Context context) {
        return Utils.getValue(context, FS.SP_PASSWORD_KEY);
    }

    public void setCurrentUser(User user) {
        this.mCurrentUser = user;
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public void addMoneyToAccount(String money) {
        if (mCurrentUser != null) {
            int curMoney = mCurrentUser.getWallet();
            int addedMoney = Integer.parseInt(money);
            mCurrentUser.setWallet(curMoney + addedMoney);
        }

        if (mCurrentFacebookUser != null) {
            int curMoney = mCurrentFacebookUser.getWallet();
            int addedMoney = Integer.parseInt(money);
            mCurrentFacebookUser.setWallet(curMoney + addedMoney);
        }
    }

    public void divMoneyFromAccount(String money) {
        if (mCurrentUser != null) {
            int curMoney = mCurrentUser.getWallet();
            int divMoney = Integer.parseInt(money);
            mCurrentUser.setWallet(curMoney - divMoney);
        }

        if (mCurrentFacebookUser != null) {
            int curMoney = mCurrentFacebookUser.getWallet();
            int divMoney = Integer.parseInt(money);
            mCurrentFacebookUser.setWallet(curMoney - divMoney);
        }
    }

    public User getCurrentFacebookUser() {
        return mCurrentFacebookUser;
    }

    public void setCurrentFacebookUser(User user) {
        this.mCurrentFacebookUser = user;
    }

    public void loadNewsList(List<Param> paramList, final Callback<NewsList> callback) {
        paramList.add(new Param("_request", "get_news_list"));

        network.execute(FS.WEB_API, paramList, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    NewsList newsList = gson.fromJson(response.getResponseObject(), NewsList.class);
                    if (newsList != null) {
                        callback.onSuccess(newsList);
                    } else {
                        callback.onFail(response.getResponseObject());
                    }
                } else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void loadStatistic(List<Param> params, final Callback<Statistic> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("Bạn chưa đăng nhập vào hệ thống của chúng tôi. Vui lòng đăng nhập");
            return;
        }

        params.add(new Param("_request", "statistics"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    Statistic statistics = gson.fromJson(response.getResponseObject(), Statistic.class);
                    if (statistics != null) {
                        callback.onSuccess(statistics);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getCustomer(List<Param> params, final Callback<Customer> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("Bạn chưa đăng nhập vào hệ thống của chúng tôi. Vui lòng đăng nhập");
            return;
        }

        params.add(new Param("_request", "get_customer"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    Customer customer = gson.fromJson(response.getResponseObject(), Customer.class);
                    if (customer != null) {
                        callback.onSuccess(customer);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void loadNews(List<Param> paramList, final Callback<String> callback) {
        paramList.add(new Param("_request", "get_news"));

        network.execute(FS.WEB_API, paramList, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void sendFeedback(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("Bạn chưa đăng nhập vào hệ thống của chúng tôi. Vui lòng đăng nhập");
            return;
        }

        params.add(new Param("_request", "send_feedback"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void login(List<Param> params, final Callback<User> callback) {

        params.add(new Param("_request", "login"));
        params.add(new Param("_version", FS.APP_VERSION));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void rating(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("Bạn chưa đăng nhập vào hệ thống của chúng tôi. Vui lòng đăng nhập");
            return;
        }

        params.add(new Param("_request", "rating"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void submitPromotionCode(List<Param> params, final Callback<User> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("Bạn chưa đăng nhập vào hệ thống của chúng tôi. Vui lòng đăng nhập");
            return;
        }

        params.add(new Param("_request", "submit_promotion"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void register(List<Param> params, final Callback<User> callback) {

        params.add(new Param("_request", "register"));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void verify(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "verify"));
        params.add(new Param("_token", currentUser.getToken()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void loginFacebook(List<Param> params, final Callback<User> callback) {
        params.add(new Param("_request", "loginface"));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void updateRegistrationID(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        mCurrentRegistrationID = FirebaseInstanceId.getInstance().getToken();

        params.add(new Param("_request", "update_registration_id"));
        params.add(new Param("_registration_id", mCurrentRegistrationID));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void add_money_by_card_number(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "add_money_by_card_number"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void add_money_to_wallet(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "add_money_to_wallet"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void update_profile(List<Param> params, final Callback<User> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "update_profile"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    User user = gson.fromJson(response.getResponseObject(), User.class);
                    if (user != null) {
                        callback.onSuccess(user);
                    }
                    else {
                        callback.onFail(response.getResponseObject());
                    }
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void update_avatar(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "upload_image"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void change_password(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "change_password"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void get_gig_ticket(List<Param> params, final Callback<GigTicketResponse> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    GigTicketResponse gigTicketResponse = gson.fromJson(response.getResponseObject(), GigTicketResponse.class);
                    callback.onSuccess(gigTicketResponse);
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void get_book_gig_ticket(List<Param> params, final Callback<GigTicketResponse> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "get_book_gig_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    GigTicketResponse gigTicketResponse = gson.fromJson(response.getResponseObject(), GigTicketResponse.class);
                    callback.onSuccess(gigTicketResponse);
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void book_gig_ticket(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "book_ticket"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void upload_certificate(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "upload_certificate"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void confirmPicked(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "confirm_picked"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void cancelPicked(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "cancel_picked"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void endTicketError(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "end_ticket_error"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void endTicketSuccess(List<Param> params, final Callback<String> callback) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            callback.onFail("not login");
            return;
        }

        params.add(new Param("_request", "end_ticket_success"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    callback.onSuccess(response.getResponseObject());
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDistance(String from, String to, final Callback<Distance> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("_request", "get_distance"));
        params.add(new Param("_from", from));
        params.add(new Param("_to", to));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    Distance distance = gson.fromJson(response.getResponseObject(), Distance.class);
                    callback.onSuccess(distance);
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDistance2(String lat, String lon, List<String> toList, final Callback<Distance> callback) {
        JSONArray jsonArray = new JSONArray(toList);

        List<Param> params = new ArrayList<>();
        params.add(new Param("_request", "get_distance2"));
        params.add(new Param("_lat", lat));
        params.add(new Param("_lon", lon));
        params.add(new Param("_to", jsonArray.toString()));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    Distance distance = gson.fromJson(response.getResponseObject(), Distance.class);
                    callback.onSuccess(distance);
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getDirection(DirectionLocation from, DirectionLocation to, final Callback<Direction> callback) {
        OkHttpClient client = new OkHttpClient();
        String url = getDirectionUrl(from, to);
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                Gson gson = new Gson();
                String body = response.body().string();
                Direction direction = gson.fromJson(body, Direction.class);
                callback.onSuccess(direction);
            }
        });
    }

    public void getLocationFromAddress(String address, final Callback<DirectionLocation> callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("_request", "get_location_from_address"));
        params.add(new Param("_address", address));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                if (response.getResult()) {
                    Gson gson = new Gson();
                    DirectionLocation directionLocation = gson.fromJson(response.getResponseObject(), DirectionLocation.class);
                    callback.onSuccess(directionLocation);
                }
                else {
                    callback.onFail(response.getResponseObject());
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail(error);
            }
        });
    }

    public void getSnapRoad(List<DirectionLocation> locations, final Callback<SnappedResponse> callback) {
        getSnapRoad(0, locations, null, callback);
    }

    private void getSnapRoad(final int index, final List<DirectionLocation> locations, final SnappedResponse snappedResponse, final Callback<SnappedResponse> callback) {
        List<DirectionLocation> data = new ArrayList<>();
        for (DirectionLocation dl : locations) {
            data.add(dl);
        }

        if ((index * 100) >= locations.size()) {
            callback.onSuccess(snappedResponse);
            return;
        }

        int startPoint = 100 * index;
        while (data.size() > (100 + startPoint)) {
            data.remove(data.size() - 1);
        }

        List<DirectionLocation> prepare = new ArrayList<>();
        for (int i = startPoint; i < data.size(); i++) {
            prepare.add(data.get(i));
        }

        OkHttpClient client = new OkHttpClient();
        String url = getSNapRoadDirectionUrl(prepare);
        if (url == null) {
            callback.onSuccess(snappedResponse);
            return;
        }
        Request request = new Request.Builder().url(url).build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                Gson gson = new Gson();
                String body = response.body().string();
                SnappedResponse _snapppedResponse = gson.fromJson(body, SnappedResponse.class);

                SnappedResponse snappedResponse1 = snappedResponse;
                if (snappedResponse1 == null) {
                    snappedResponse1 = new SnappedResponse();
                    snappedResponse1.setSnappedPoints(_snapppedResponse.getSnappedPoints());
                }
                else {
                    snappedResponse1.addSnappedPoints(_snapppedResponse.getSnappedPoints());
                }

                getSnapRoad(index + 1, locations, snappedResponse1, callback);
            }
        });
    }

    private String getDirectionUrl(DirectionLocation origin, DirectionLocation dest) {
        String str_origin = "origin=" + origin.getLat() + "," + origin.getLng();
        String str_dest = "destination=" + dest.getLat() + "," + dest.getLng();
        String sensor = "&mode=driving&sensor=true";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    private String getSNapRoadDirectionUrl(List<DirectionLocation> locations) {
        String content = "";
        for (DirectionLocation dl : locations) {
            content += dl.getLat() + "," + dl.getLng() + "|";
        }

        if (content.length() == 0) {
            return null;
        }

        content = content.substring(0, content.length() - 1);
        String url = "https://roads.googleapis.com/v1/snapToRoads?path=" + content + "&interpolate=true&key=AIzaSyC6a-f4U-7gRozymjlxJIgXZ9zb7QUVl6o";
        return url;
    }

    public void sendPosition(double lat, double lon) {
        User currentUser = mCurrentUser;
        if (currentUser == null) {
            currentUser = mCurrentFacebookUser;
        }
        if (currentUser == null) {
            return;
        }

        List<Param> params = new ArrayList<>();
        params.add(new Param("_request", "send_position"));
        params.add(new Param("_token", currentUser.getToken()));
        params.add(new Param("_id", currentUser.getAccountID()));
        params.add(new Param("_lat", lat + ""));
        params.add(new Param("_lon", lon + ""));

        network.execute(FS.WEB_API, params, new Network.Callback() {
            @Override
            public void onCallBack(Response response) {
                //TODO
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    };
}
