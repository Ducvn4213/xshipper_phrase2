package product.dg.xshipper.certificate.card;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapRegionDecoder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import product.dg.xshipper.R;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.util.FS;

public class CardUploadActivity extends AppCompatActivity {

    enum TYPE {
        CMND,
        GPLX
    }

    Bitmap mFrontSide;
    Bitmap mBackSide;
    ImageView mFront;
    ImageView mBack;
    Button mOK;

    TYPE mType;

    Boolean mDontCheckFront = false;
    Boolean mDontCheckBack = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_upload);

        bindingControls();
        setupControlEvents();

        init();
    }

    void init() {
        String cardType = getIntent().getStringExtra(FS.CARD_TYPE);
        if (cardType.equalsIgnoreCase(FS.CARD_CMND)) {
            CardUploadSingleTon.getInstance().clearCMNDDontCheck();
            mType = TYPE.CMND;
            CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
            Bitmap cmndFront = cardUploadSingleTon.getCMNDFrontSide();
            Bitmap cmndBack = cardUploadSingleTon.getCMNDBackSide();

            if (cmndFront != null) {
                mFront.setImageBitmap(cmndFront);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_cmndfront.jpg";

                Picasso.with(CardUploadActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mFront, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheckFront = true;
                    }

                    @Override
                    public void onError() {
                        mFront.setImageResource(R.drawable.no_image);
                    }
                });
            }

            if (cmndBack != null) {
                mBack.setImageBitmap(cmndBack);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_cmndback.jpg";

                Picasso.with(CardUploadActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mBack, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheckBack = true;
                    }

                    @Override
                    public void onError() {
                        mBack.setImageResource(R.drawable.no_image);
                    }
                });
            }
        }
        else {
            CardUploadSingleTon.getInstance().clearGPLXDontCheck();
            mType = TYPE.GPLX;
            CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
            Bitmap gplxFront = cardUploadSingleTon.getGPLXFrontSide();
            Bitmap gplxBack = cardUploadSingleTon.getGPLXBackSide();

            if (gplxFront != null) {
                mFront.setImageBitmap(gplxFront);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_gplxfront.jpg";

                Picasso.with(CardUploadActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mFront, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheckFront = true;
                    }

                    @Override
                    public void onError() {
                        mFront.setImageResource(R.drawable.no_image);
                    }
                });
            }

            if (gplxBack != null) {
                mBack.setImageBitmap(gplxBack);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_gplxback.jpg";

                Picasso.with(CardUploadActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mBack, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheckBack = true;
                    }

                    @Override
                    public void onError() {
                        mBack.setImageResource(R.drawable.no_image);
                    }
                });
            }
        }
    }

    void bindingControls() {
        mFront = (ImageView) findViewById(R.id.imv_front);
        mBack = (ImageView) findViewById(R.id.imv_back);
        mOK = (Button) findViewById(R.id.btn_ok);
    }

    void setupControlEvents() {
        mFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(CardUploadActivity.this, Intent.createChooser(intent, "Select Picture"), FS.PICK_IMAGE_FRONT_REQUEST_CODE, null);
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(CardUploadActivity.this, Intent.createChooser(intent, "Select Picture"), FS.PICK_IMAGE_BACK_REQUEST_CODE, null);
            }
        });

        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFrontSide == null && mDontCheckFront == false) {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_front_side_empty);
                    return;
                }

                if (mBackSide == null && mDontCheckBack == false) {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_back_side_empty);
                    return;
                }

                CardUploadSingleTon cardUploadSingleTon = CardUploadSingleTon.getInstance();
                if (mType == TYPE.CMND) {
                    cardUploadSingleTon.setCMNDFrontSide(mFrontSide);
                    cardUploadSingleTon.setCMNDBackSide(mBackSide);
                    cardUploadSingleTon.mDontCheckCMNDFront = mDontCheckFront;
                    cardUploadSingleTon.mDontCheckCMNDBack = mDontCheckBack;
                }
                else {
                    cardUploadSingleTon.setGPLXFrontSide(mFrontSide);
                    cardUploadSingleTon.setGPLXBackSide(mBackSide);
                    cardUploadSingleTon.mDontCheckGPLXFront = mDontCheckFront;
                    cardUploadSingleTon.mDontCheckGPLXBack = mDontCheckBack;
                }

                CardUploadActivity.this.finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FS.PICK_IMAGE_FRONT_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
            changeImage(true, filePath);
        }

        if (requestCode == FS.PICK_IMAGE_BACK_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
            changeImage(false, filePath);
        }
    }

    void changeImage(Boolean isFront, Uri filePath) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
            if (isFront) {
                Picasso.with(CardUploadActivity.this).load(filePath).into(mFront);
                mFrontSide = bitmap;
            }
            else {
                Picasso.with(CardUploadActivity.this).load(filePath).into(mBack);
                mBackSide = bitmap;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(CardUploadActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}

