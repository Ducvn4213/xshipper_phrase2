package product.dg.xshipper.certificate.another_certificate;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class AnotherCertificateSingleTon {

    private Bitmap mCNHK;
    private Bitmap mBLNT;

    public Boolean mDontCheckCNHK = false;
    public Boolean mDontCheckBLNT = false;

    private static AnotherCertificateSingleTon instance;
    private AnotherCertificateSingleTon() {}

    public static AnotherCertificateSingleTon getInstance() {
        if (instance == null) {
            instance = new AnotherCertificateSingleTon();
        }

        return instance;
    }

    public void clearCNHKDontCheck() {
        mDontCheckCNHK = false;
    }

    public void clearBLNTDontCheck() {
        mDontCheckBLNT = false;
    }

    public void clearCNHKList() {
        mCNHK = null;
    }

    public void clearBLNTList() {
        mBLNT = null;
    }

    public void setCNHK(Bitmap bitmap) {
        mCNHK = bitmap;
    }

    public void setBLNT(Bitmap bitmap) {
        mBLNT = bitmap;
    }

    public Bitmap getCNHK() {
        return mCNHK;
    }

    public Bitmap getBLNT() {
        return mBLNT;
    }
}
