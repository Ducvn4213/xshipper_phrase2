package product.dg.xshipper.certificate.card;

import android.graphics.Bitmap;

public class CardUploadSingleTon {

    private Bitmap mCMNDFrontSideBitmap;
    private Bitmap mCMNDBackSideBitmap;

    public Boolean mDontCheckCMNDFront = false;
    public Boolean mDontCheckCMNDBack = false;

    private Bitmap mGPLXBackSideBitmap;
    private Bitmap mGPLXFrontSideBitmap;

    public Boolean mDontCheckGPLXFront = false;
    public Boolean mDontCheckGPLXBack = false;


    private static CardUploadSingleTon instance;
    private CardUploadSingleTon() {}

    public static CardUploadSingleTon getInstance() {
        if (instance == null) {
            instance = new CardUploadSingleTon();
        }
        return instance;
    }

    public void clearCMNDDontCheck() {
        mDontCheckCMNDBack = false;
        mDontCheckCMNDFront = false;
    }

    public void clearGPLXDontCheck() {
        mDontCheckGPLXBack = false;
        mDontCheckGPLXFront = false;
    }

    public void clearCMND() {
        mCMNDFrontSideBitmap = null;
        mCMNDBackSideBitmap = null;
    }

    public void clearGPLX() {
        mGPLXFrontSideBitmap = null;
        mGPLXBackSideBitmap = null;
    }

    public void setCMNDFrontSide(Bitmap bitmap) {
        mCMNDFrontSideBitmap = bitmap;
    }

    public void setCMNDBackSide(Bitmap bitmap) {
        mCMNDBackSideBitmap = bitmap;
    }

    public Bitmap getCMNDFrontSide() {
        return mCMNDFrontSideBitmap;
    }

    public Bitmap getCMNDBackSide() {
        return mCMNDBackSideBitmap;
    }

    public void setGPLXFrontSide(Bitmap bitmap) {
        mGPLXFrontSideBitmap = bitmap;
    }

    public void setGPLXBackSide(Bitmap bitmap) {
        mGPLXBackSideBitmap = bitmap;
    }

    public Bitmap getGPLXFrontSide() {
        return mGPLXFrontSideBitmap;
    }

    public Bitmap getGPLXBackSide() {
        return mGPLXBackSideBitmap;
    }
}
