package product.dg.xshipper.certificate.another_certificate;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback;

import java.io.IOException;

import product.dg.xshipper.R;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.util.FS;

import static product.dg.xshipper.util.FS.PICK_IMAGE_REQUEST_CODE;

public class AnotherCertificateActivity extends AppCompatActivity {

    enum TYPE {
        CNHK,
        BLNT
    }

    ImageView mAdd;
    Button mOK;

    TYPE mType;

    Bitmap mData;

    Boolean mDontCheck = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_certificate_upload);

        bindingControls();
        setupControlEvents();

        init();
    }

    void bindingControls() {
        mAdd = (ImageView) findViewById(R.id.imv_add);
        mOK = (Button) findViewById(R.id.btn_ok);
    }

    void setupControlEvents() {
        mAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(AnotherCertificateActivity.this, Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE, null);
            }
        });

        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();
                if (mData == null && mDontCheck == false) {
                    showDialog(R.string.dialog_title_error, R.string.dialog_message_image_side_empty);
                    return;
                }

                if (mType == TYPE.CNHK) {
                    anotherCertificateSingleTon.setCNHK(mData);
                    anotherCertificateSingleTon.mDontCheckCNHK = mDontCheck;
                }
                else {
                    anotherCertificateSingleTon.setBLNT(mData);
                    anotherCertificateSingleTon.mDontCheckBLNT = mDontCheck;
                }

                AnotherCertificateActivity.this.finish();
            }
        });
    }

    void init() {
        String cardType = getIntent().getStringExtra(FS.CERTIFICATE_TYPE);
        if (cardType.equalsIgnoreCase(FS.CERTIFICATE_CNHK)) {
            AnotherCertificateSingleTon.getInstance().clearCNHKDontCheck();
            mType = TYPE.CNHK;

            AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();
            Bitmap cnhk = anotherCertificateSingleTon.getCNHK();

            if (cnhk != null) {
                mAdd.setImageBitmap(cnhk);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_cnhk.jpg";

                Picasso.with(AnotherCertificateActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mAdd, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheck = true;
                    }

                    @Override
                    public void onError() {
                        mAdd.setImageResource(R.drawable.no_image);
                    }
                });
            }
        }
        else {
            AnotherCertificateSingleTon.getInstance().clearBLNTDontCheck();
            mType = TYPE.BLNT;

            AnotherCertificateSingleTon anotherCertificateSingleTon = AnotherCertificateSingleTon.getInstance();
            Bitmap blnt = anotherCertificateSingleTon.getBLNT();

            if (blnt != null) {
                mAdd.setImageBitmap(blnt);
            }
            else {
                String id = FSService.getInstance().getCurrentActiveUser().getAccountID();
                String link = FS.IMAGE_HOST + "uploadedimages/" + id + "_blnt.jpg";

                Picasso.with(AnotherCertificateActivity.this).load(link).memoryPolicy(MemoryPolicy.NO_CACHE).into(mAdd, new Callback() {
                    @Override
                    public void onSuccess() {
                        mDontCheck = true;
                    }

                    @Override
                    public void onError() {
                        mAdd.setImageResource(R.drawable.no_image);
                    }
                });
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri filePath = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                mData = bitmap;
                mAdd.setImageBitmap(mData);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(AnotherCertificateActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
