package product.dg.xshipper.certificate;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;

public class CertificateInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void onSuccess();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);

        void goToCMND();
        void goToGPLX();
        void goToHK();
        void goToBLNT();

        void upload();
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void uploadSuccess();
        void uploadFail();
    }

    interface ProvidedModelOps {
        void upload();
    }
}