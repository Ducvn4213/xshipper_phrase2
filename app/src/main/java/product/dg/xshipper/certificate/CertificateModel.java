package product.dg.xshipper.certificate;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.certificate.another_certificate.AnotherCertificateSingleTon;
import product.dg.xshipper.certificate.card.CardUploadSingleTon;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;
import product.dg.xshipper.util.Utils;

public class CertificateModel implements CertificateInterfaces.ProvidedModelOps {

    private interface convertTaskCallback {
        void onSuccess();
        void onFail();
    }

    private CertificateInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    private String mCMNDFront;
    private String mCMNDBack;
    private String mGPLXFront;
    private String mGPLXBack;
    private String mCNHK;
    private String mBLNT;


    CertificateModel(CertificateInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    private Bitmap getBitmapByID(int id) {
        switch (id) {
            case 1:
                return CardUploadSingleTon.getInstance().getCMNDFrontSide();
            case 2:
                return CardUploadSingleTon.getInstance().getCMNDBackSide();
            case 3:
                return CardUploadSingleTon.getInstance().getGPLXFrontSide();
            case 4:
                return CardUploadSingleTon.getInstance().getGPLXBackSide();
            case 5:
                return AnotherCertificateSingleTon.getInstance().getCNHK();
            case 6:
                return AnotherCertificateSingleTon.getInstance().getBLNT();
            default:
                break;
        }

        return null;
    }

    @Override
    public void upload() {
        convertTask(1, new convertTaskCallback() {
            @Override
            public void onSuccess() {
                doUpload();
            }

            @Override
            public void onFail() {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_something_error);
            }
        });
    }

    private void convertTask(final int tag, final convertTaskCallback callback) {
        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(getBitmapByID(tag));
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @Override
            public void onSuccess(final String string) {
            switch (tag) {
                    case 1:
                        mCMNDFront = string;
                        break;
                    case 2:
                        mCMNDBack = string;
                        break;
                    case 3:
                        mGPLXFront = string;
                        break;
                    case 4:
                        mGPLXBack = string;
                        break;
                    case 5:
                        mCNHK = string;
                        break;
                    case 6:
                        mBLNT = string;
                        break;
                    default:
                        break;
                }

                if (tag == 6) {
                    callback.onSuccess();
                    return;
                }
                else {
                    int _tag = tag + 1;
                    convertTask(_tag, callback);
                }
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
        convertBitmapToStringTask.execute();
    }

    private void doUpload() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_cmnd_front", mCMNDFront));
        paramList.add(new Param("_cmnd_back", mCMNDBack));
        paramList.add(new Param("_gplx_front", mGPLXFront));
        paramList.add(new Param("_gplx_back", mGPLXBack));
        paramList.add(new Param("_cnhk", mCNHK));
        paramList.add(new Param("_blnt", mBLNT));

        mService.upload_certificate(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.uploadSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.dialog_message_something_error);
            }
        });
    }
}
