package product.dg.xshipper.certificate;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import product.dg.xshipper.R;
import product.dg.xshipper.certificate.another_certificate.AnotherCertificateActivity;
import product.dg.xshipper.certificate.card.CardUploadActivity;
import product.dg.xshipper.certificate.card.CardUploadSingleTon;
import product.dg.xshipper.change_password.ChangePasswordInterfaces;
import product.dg.xshipper.util.FS;

public class CertificatePresenter implements CertificateInterfaces.ProvidedPresenterOps, CertificateInterfaces.RequiredPresenterOps {
    private WeakReference<CertificateInterfaces.RequiredViewOps> mView;
    private CertificateInterfaces.ProvidedModelOps mModel;

    CertificatePresenter(CertificateInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(CertificateInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(CertificateInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void goToCMND() {
        Intent intent = new Intent(mView.get().getActivity(), CardUploadActivity.class);
        intent.putExtra(FS.CARD_TYPE, FS.CARD_CMND);
        ActivityCompat.startActivityForResult(mView.get().getActivity(), intent, FS.PICK_CMND_REQUEST_CODE, null);
    }

    @Override
    public void goToGPLX() {
        Intent intent = new Intent(mView.get().getActivity(), CardUploadActivity.class);
        intent.putExtra(FS.CARD_TYPE, FS.CARD_GPLX);
        ActivityCompat.startActivityForResult(mView.get().getActivity(), intent, FS.PICK_GPLX_REQUEST_CODE, null);
    }

    @Override
    public void goToHK() {
        Intent intent = new Intent(mView.get().getActivity(), AnotherCertificateActivity.class);
        intent.putExtra(FS.CERTIFICATE_TYPE, FS.CERTIFICATE_CNHK);
        ActivityCompat.startActivityForResult(mView.get().getActivity(), intent, FS.PICK_CNHK_REQUEST_CODE, null);
    }

    @Override
    public void goToBLNT() {
        Intent intent = new Intent(mView.get().getActivity(), AnotherCertificateActivity.class);
        intent.putExtra(FS.CERTIFICATE_TYPE, FS.CERTIFICATE_BLNT);
        ActivityCompat.startActivityForResult(mView.get().getActivity(), intent, FS.PICK_BLNT_REQUEST_CODE, null);
    }

    @Override
    public void upload() {
        mView.get().showWaiting();
        mModel.upload();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void uploadSuccess() {
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onSuccess();
            }
        });
    }

    @Override
    public void uploadFail() {

    }
}
