package product.dg.xshipper.news.news_list;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.model.News;

public class NewsListPresenter implements NewsListInterfaces.ProvidedPresenterOps, NewsListInterfaces.RequiredPresenterOps{
    private WeakReference<NewsListInterfaces.RequiredViewOps> mView;
    private NewsListInterfaces.ProvidedModelOps mModel;

    NewsListPresenter(NewsListInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(NewsListInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(NewsListInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void loadData(String cate) {
        mView.get().showWaiting();
        mModel.loadData(cate);
    }

    @Override
    public News getData(int pos) {
        return mModel.getData(pos);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void presentData(final List<News> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentData(data);
            }
        });
    }
}
