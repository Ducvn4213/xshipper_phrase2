package product.dg.xshipper.news.news_list;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.model.NewsList;
import product.dg.xshipper.model.News;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;


public class NewsListModel implements NewsListInterfaces.ProvidedModelOps {
    private NewsListInterfaces.RequiredPresenterOps mPresenter;
    private FSService mService = FSService.getInstance();
    private List<News> mData;
    NewsListModel(NewsListInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData(String cate) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_cate", cate));

        mService.loadNewsList(paramList, new FSService.Callback<NewsList>() {
            @Override
            public void onSuccess(NewsList data) {
                mData = data.getData();
                mPresenter.presentData(data.getData());
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public News getData(int pos) {
        return mData.get(pos);
    }
}
