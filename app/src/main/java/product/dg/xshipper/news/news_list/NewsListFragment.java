package product.dg.xshipper.news.news_list;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.NewsAdapter;
import product.dg.xshipper.model.News;
import product.dg.xshipper.news.news_detail.NewsActivity;


public class NewsListFragment extends Fragment implements NewsListInterfaces.RequiredViewOps {
    ListView mListView;
    private NewsListInterfaces.ProvidedPresenterOps mPresenter;
    private ProgressDialog mProgressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_news_cate_list, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        mListView = (ListView) view.findViewById(R.id.news_list);
    }

    void setupControlEvents() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News data = mPresenter.getData(position);
                Intent intent = new Intent(getParentActivity(), NewsActivity.class);
                intent.putExtra("id", data.getID());
                intent.putExtra("date", data.getDate());
                intent.putExtra("summary", data.getSummary());
                intent.putExtra("title", data.getTitle());
                startActivity(intent);
            }
        });
    }

    void init() {
        NewsListPresenter presenter = new NewsListPresenter(this);
        NewsListModel model = new NewsListModel(presenter);

        presenter.setModel(model);
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData("1");
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) NewsListFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
            }
        });
    }

    @Override
    public void hideWaiting() {
        getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void presentData(List<News> data) {
        NewsAdapter adapter = new NewsAdapter(getParentActivity(), data);
        mListView.setAdapter(adapter);
    }
}
