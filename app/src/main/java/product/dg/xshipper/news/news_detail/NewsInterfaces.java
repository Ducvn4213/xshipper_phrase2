package product.dg.xshipper.news.news_detail;

import android.support.v7.app.AppCompatActivity;

public class NewsInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(String data);
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData(String id);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void presentData(String data);
    }

    interface ProvidedModelOps {
        void loadData(String id);
    }
}
