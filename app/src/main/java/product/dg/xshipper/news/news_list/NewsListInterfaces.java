package product.dg.xshipper.news.news_list;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.model.News;

public class NewsListInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void presentData(List<News> data);
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData(String cate);
        News getData(int pos);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void presentData(List<News> data);
    }

    interface ProvidedModelOps {
        void loadData(String cate);
        News getData(int pos);
    }
}
