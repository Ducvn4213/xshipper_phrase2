package product.dg.xshipper.gig_ticket;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import product.dg.xshipper.R;
import product.dg.xshipper.model.GigTicket;

public class GigTicketFragment extends Fragment implements ViewPager.OnPageChangeListener {
    GigTicketPagerAdapter mPagerAdapter;
    ViewPager mViewPager;

    private static GigTicketFragment instance;

    public static GigTicketFragment getInstance () {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gig_ticket, container, false);

        bindingControls(view);
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = GigTicketFragment.this;
    }

    public int getCurrentPage() {
        return mViewPager.getCurrentItem();
    }

    void bindingControls(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    void init() {
        mPagerAdapter = new GigTicketPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 1) {
            GigTicketListFragment.getInstance().onResume();
        }
        else if (position == 0) {
            GigTicketMapFragment.getInstance().refreshData();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public void changeToList() {
        mViewPager.setCurrentItem(1);
    }

    public void changeToMap() {
        mViewPager.setCurrentItem(0);
    }

    private class GigTicketPagerAdapter extends FragmentPagerAdapter {

        public GigTicketPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {
                case 0: return new GigTicketMapFragment();
                case 1: return new GigTicketListFragment();
                default : return new GigTicketMapFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
