package product.dg.xshipper.gig_ticket;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

import product.dg.xshipper.MainActivity;
import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.adapter.PresentGigTicketAdapter;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketGroup;
import product.dg.xshipper.model.PresentGigTicket;
import product.dg.xshipper.util.Utils;

public class GigTicketListFragment extends Fragment implements GigTicketInterfaces.RequiredViewOps {

    ProgressDialog mProgressDialog;
    ListView mGigList;
    TextView mNoGig;

    private GigTicketInterfaces.ProvidedPresenterOps mPresenter;

    private AlertDialog mCurDialog;

    private static GigTicketListFragment instance;

    public static GigTicketListFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gig_ticket_list, container, false);

        bindingUI(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();

        instance = GigTicketListFragment.this;
    }

    void init() {
        GigTicketListPresenter presenter = new GigTicketListPresenter(this);
        GigTicketListModel model = new GigTicketListModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    void bindingUI(View view) {
        mGigList = (ListView) view.findViewById(R.id.gig_ticket_list);
        mNoGig = (TextView) view.findViewById(R.id.no_ticket);
    }

    void setupControlEvents() {
        mGigList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final PresentGigTicket data = mPresenter.getDataFromPosition(i);
                if (data == null) {
                    return;
                }

                if (data.getIsGroup() == false) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
                    LayoutInflater inflater = getParentActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.layout_view_gig_item, null);
                    dialogBuilder.setView(dialogView);

                    TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                    TextView cod = (TextView) dialogView.findViewById(R.id.edt_cod);
                    TextView size = (TextView) dialogView.findViewById(R.id.edt_size);
                    TextView from = (TextView) dialogView.findViewById(R.id.edt_from);
                    TextView to = (TextView) dialogView.findViewById(R.id.edt_to);
                    TextView est = (TextView) dialogView.findViewById(R.id.edt_estimate);

                    Button book = (Button) dialogView.findViewById(R.id.btn_book);
                    Button viewDetail = (Button) dialogView.findViewById(R.id.btn_viewdetail);

                    final GigTicket _data = data.getGigTicket();
                    name.setText(_data.getTitle());
                    size.setText("Size: " + _data.getSize());
                    from.setText("From: " + _data.getFrom());
                    to.setText("To: " + _data.getTo());
                    est.setText("Distance: " + _data.getDistance());

                    if (_data.getCOD() == null || _data.getCOD().trim().equalsIgnoreCase("")) {
                        cod.setText(getString(R.string.gig_ticket_nocod));
                    }
                    else {
                        cod.setText(getString(R.string.gig_ticket_cod));
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();

                    book.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mPresenter.book(_data.getId());
                        }
                    });

                    viewDetail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });

                    if (!alertDialog.isShowing()) {
                        mCurDialog = alertDialog;
                        alertDialog.show();
                    }
                }
                else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
                    LayoutInflater inflater = getParentActivity().getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.layout_view_group_gig_item, null);
                    dialogBuilder.setView(dialogView);

                    TextView name = (TextView) dialogView.findViewById(R.id.edt_name);
                    ListView listGroup = (ListView) dialogView.findViewById(R.id.listGroup);

                    Button book = (Button) dialogView.findViewById(R.id.btn_book);
                    Button viewDetail = (Button) dialogView.findViewById(R.id.btn_viewdetail);

                    final GigTicketGroup _data = data.getGigTicketGroup();
                    name.setText(_data.getTitle());

                    GigTicketAdapter _adapter = new GigTicketAdapter(getParentActivity(), _data.getData());
                    listGroup.setAdapter(_adapter);

                    final AlertDialog alertDialog = dialogBuilder.create();

                    book.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mPresenter.book(_data);
                        }
                    });

                    viewDetail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                        }
                    });

                    if (!alertDialog.isShowing()) {
                        mCurDialog = alertDialog;
                        alertDialog.show();
                    }
                }
            }
        });
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) GigTicketListFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void updateAdapter(PresentGigTicketAdapter adapter) {
        if (mGigList != null) {
            mGigList.setAdapter(adapter);
        }
    }

    @Override
    public void presentData(List<PresentGigTicket> data) {

    }

    @Override
    public void bookSuccess() {
        mPresenter.loadData();
        mCurDialog.dismiss();
        hideWaiting();
        MainActivity mainActivity = (MainActivity) getParentActivity();
        mainActivity.goToTab(1);

        showDialog(R.string.dialog_notice_title, R.string.gig_ticket_book_success);
    }

    @Override
    public void bookFail() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_book_fail);
    }

    @Override
    public void onNoTicket() {
        mNoGig.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHaveTicket() {
        mNoGig.setVisibility(View.GONE);
    }

    @Override
    public void onEmptyOfWallet() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.dialog_message_empty_money);
    }

    @Override
    public void onDeactiveAccount() {
        mCurDialog.dismiss();
        hideWaiting();
        showDialog(R.string.dialog_title_error, R.string.dialog_message_deactive_account);
    }
}
