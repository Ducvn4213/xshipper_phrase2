package product.dg.xshipper.gig_ticket;

import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.adapter.PresentGigTicketAdapter;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketGroup;
import product.dg.xshipper.model.PresentGigTicket;
import product.dg.xshipper.model.User;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class GigTicketListModel implements GigTicketInterfaces.ProvidedModelOps {
    private GigTicketInterfaces.RequiredPresenterOps mPresenter;
    private FSService mService = FSService.getInstance();

    List<PresentGigTicket> mData = new ArrayList<>();
    PresentGigTicketAdapter mAdapter;

    GigTicketListModel(GigTicketInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
        mAdapter = mPresenter.createAdapterFromData(mData);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void loadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "1"));

        mService.get_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mData = data.getPresentData();
                mPresenter.updateData(mData);
                mPresenter.onHaveTicket();
            }

            @Override
            public void onFail(String error) {
                mData.clear();
                mPresenter.updateData(mData);
                mPresenter.onNoTicket();
            }
        });
    }

    @Override
    public void notifyChange(List<PresentGigTicket> data) {
        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public PresentGigTicket getDataFromPosition(int pos) {
        return mData.get(pos);
    }

    @Override
    public void bookATicket(int id) {
        User currentUser = mService.getCurrentActiveUser();

        if (!currentUser.isActive() || !currentUser.isVerify()) {
            mPresenter.onDeactiveAccount();
            return;
        }

        if (currentUser.getWallet() <= 0) {
            mPresenter.onEmptyOfWallet();
            return;
        }

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", id + ""));

        mService.book_gig_ticket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.bookSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.bookFail();
            }
        });
    }

    @Override
    public void book(GigTicketGroup group) {
        int expMoney = group.getData().size() * 2000;

        User currentUser = mService.getCurrentActiveUser();

        if (!currentUser.isActive() || !currentUser.isVerify()) {
            mPresenter.onDeactiveAccount();
            return;
        }

        if (currentUser.getWallet() < expMoney) {
            mPresenter.onEmptyOfWallet();
            return;
        }

        List<GigTicket> clone = new ArrayList<>();
        for (GigTicket gt : group.getData()) {
            clone.add(gt);
        }

        bookThatList(clone);
    }

    private void bookThatList(final List<GigTicket> group) {
        if (group == null) {
            mPresenter.bookFail();
            return;
        }
        if (group.size() == 0) {
            mPresenter.bookSuccess();
            return;
        }

        GigTicket one = group.get(0);

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", one.getId() + ""));

        mService.book_gig_ticket(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                group.remove(0);
                bookThatList(group);
            }

            @Override
            public void onFail(String error) {
                bookThatList(null);
            }
        });
    }
}
