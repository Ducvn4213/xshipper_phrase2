package product.dg.xshipper.gig_ticket;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.adapter.PresentGigTicketAdapter;
import product.dg.xshipper.model.GigTicketGroup;
import product.dg.xshipper.model.PresentGigTicket;

interface GigTicketInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateAdapter(PresentGigTicketAdapter adapter);
        void presentData(List<PresentGigTicket> data);

        void bookSuccess();
        void bookFail();

        void onNoTicket();
        void onHaveTicket();

        void onEmptyOfWallet();
        void onDeactiveAccount();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void loadData();

        void book(int id);
        void book(GigTicketGroup group);
        PresentGigTicket getDataFromPosition(int id);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        PresentGigTicketAdapter createAdapterFromData(List<PresentGigTicket> data);
        void updateAdapter(PresentGigTicketAdapter adapter);
        void presentData(List<PresentGigTicket> data);
        void updateData(List<PresentGigTicket> data);

        void bookSuccess();
        void bookFail();

        void onNoTicket();
        void onHaveTicket();

        void onEmptyOfWallet();

        void onDeactiveAccount();
    }

    interface ProvidedModelOps {
        void loadData();
        void notifyChange(List<PresentGigTicket> data);
        PresentGigTicket getDataFromPosition(int id);
        void bookATicket(int id);
        void book(GigTicketGroup group);
    }
}