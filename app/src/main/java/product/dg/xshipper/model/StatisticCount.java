package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class StatisticCount {
    @SerializedName("id")
    String mID;
    @SerializedName("create_date")
    String mCreateDate;

    public String getID() {
        return mID;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public void setCreateDate(String data) {
        this.mCreateDate = data;
    }

}
