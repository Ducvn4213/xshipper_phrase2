package product.dg.xshipper.model;

import java.util.List;

public class PresentGigTicket {
    private boolean mIsGroup;

    private GigTicket mGigTicket;
    private GigTicketGroup mGigTicketGroup;

    public PresentGigTicket() {
        mIsGroup = false;
    }

    public boolean getIsGroup() {
        return mIsGroup;
    }

    public void setIsGroup(boolean data) {
        mIsGroup = data;
    }

    public void setGigTicket(GigTicket data) {
        mGigTicket = data;
    }
    public void setGigTicketGroup(GigTicketGroup data) {
        mGigTicketGroup = data;
    }

    public GigTicket getGigTicket() {
        return mGigTicket;
    }
    public GigTicketGroup getGigTicketGroup() {
        return mGigTicketGroup;
    }
}
