package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SnappedResponse {
    @SerializedName("snappedPoints")
    List<SnappedPoint> mSnappedPoints;

    public SnappedResponse() {}

    public List<SnappedPoint> getSnappedPoints() {
        return mSnappedPoints;
    }

    public void addSnappedPoints(List<SnappedPoint> points) {
        mSnappedPoints.addAll(points);
    }

    public void setSnappedPoints(List<SnappedPoint> points) {
        mSnappedPoints = points;
    }
}
