package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class Distance {
    @SerializedName("distance")
    int mDistance;
    @SerializedName("lat")
    double mLat;
    @SerializedName("lon")
    double mLon;
    @SerializedName("index")
    int mIndex;

    public Distance() {}

    public int getDistance() {
        return mDistance;
    }

    public double getLatitude() {
        return mLat;
    }

    public double getLongitude() {
        return mLon;
    }

    public int getIndex() {
        return  mIndex;
    }
}
