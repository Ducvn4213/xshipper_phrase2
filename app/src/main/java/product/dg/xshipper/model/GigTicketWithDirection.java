package product.dg.xshipper.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

public class GigTicketWithDirection {
    GigTicket mGigTicket;
    LatLng mLocation;

    public GigTicketWithDirection() {}

    public void setLocation(LatLng data) {
        mLocation = data;
    };

    public void setGigTicket(GigTicket data) {
        mGigTicket = data;
    }

    public LatLng getLocation() {
        return mLocation;
    }

    public GigTicket getGigTicket() {
        return mGigTicket;
    }
}
