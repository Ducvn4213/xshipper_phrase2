package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class StatisticIncome {
    @SerializedName("id")
    String mID;
    @SerializedName("create_date")
    String mCreateDate;
    @SerializedName("money")
    String mMoney;


    public String getID() {
        return mID;
    }

    public String getCreateDate() {
        return mCreateDate;
    }

    public String getMoney() {
        return mMoney;
    }

    public void setCreateDate(String data) {
        this.mCreateDate = data;
    }
}
