package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class NewsDetail {
    @SerializedName("id")
    String mID;
    @SerializedName("content")
    String mContent;
    @SerializedName("id_categorynew")
    String mCategory;
    @SerializedName("date")
    String mDate;
    @SerializedName("summary")
    String mSummary;
    @SerializedName("title")
    String mTitle;
    @SerializedName("url")
    String mUrl;
    @SerializedName("view")
    String mView;
    @SerializedName("slug")
    String mSlug;

    public String getID() {
        return mID;
    }
    public String getContent() {
        return mContent;
    }
    public String getCategory() {
        return mCategory;
    }
    public String getDate() {
        return mDate;
    }
    public String getSummary() {
        return mSummary;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getUrl() {
        return mUrl;
    }
    public String getView() {
        return mView;
    }
    public String getSlug() {
        return mSlug;
    }
}
