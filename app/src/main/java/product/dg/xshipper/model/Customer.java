package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class Customer {
    @SerializedName("name")
    String mName;
    @SerializedName("email")
    String mEmail;
    @SerializedName("count")
    String mCount;
    @SerializedName("phone")
    String mPhone;
    @SerializedName("rating")
    String mRating;
    @SerializedName("avatar")
    String mAvatar;

    public Customer() {}

    public String getName() {
        return mName;
    }
    public String getEmail() {
        return mEmail;
    }
    public String getCount() {
        return mCount;
    }
    public String getPhone() {
        return mPhone;
    }
    public String getRating() {
        return mRating;
    }
    public String getAvatar() {
        return mAvatar;
    }
}
