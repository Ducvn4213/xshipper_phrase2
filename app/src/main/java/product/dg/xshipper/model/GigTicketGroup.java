package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GigTicketGroup {
    private String mTitle;
    private String mSize;
    private String mFrom;
    private String mTo;

    private List<GigTicket> mData;

    public GigTicketGroup() {}

    public void setSize(String data) {
        mSize = data;
    }
    public void setTitle(String data) {
        mTitle = data;
    }
    public void setFrom(String data) {
        mFrom = data;
    }
    public void setTo(String data) {
        mTo = data;
    }
    public void setData(List<GigTicket> data) {
        mData = data;
    }

    public String getSize() {
        return mSize;
    }
    public String getTitle() {
        return mTitle;
    }
    public String getFrom() {
        return mFrom;
    }
    public String getTo() {
        return mTo;
    }
    public List<GigTicket> getData() {
        return mData;
    }
}
