package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DirectionStep {
    @SerializedName("polyline")
    DirectionPolyline mPolyline;

    public DirectionStep () {}

    public DirectionPolyline getPolyline() {
        return mPolyline;
    }
}
