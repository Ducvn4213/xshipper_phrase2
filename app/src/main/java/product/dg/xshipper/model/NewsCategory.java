package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class NewsCategory {
    @SerializedName("id")
    String mID;
    @SerializedName("name")
    String mName;
    @SerializedName("slug")
    String mSlug;

    public String getID() {
        return mID;
    }
    public String getName() {
        return mName;
    }
    public String getSlug() {
        return mSlug;
    }
}
