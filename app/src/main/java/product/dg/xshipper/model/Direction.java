package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Direction {
    @SerializedName("routes")
    List<DirectionRoute> mDirectionRoutes;

    public Direction() {}

    public List<DirectionRoute> getDirectionRoutes() {
        return mDirectionRoutes;
    }
}
