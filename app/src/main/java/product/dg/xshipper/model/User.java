package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id")
    String mID;
    @SerializedName("account")
    String mAccount;
    @SerializedName("address")
    String mAddress;
    @SerializedName("email")
    String mEmail;
    @SerializedName("name")
    String mName;
    @SerializedName("phone")
    String mPhone;
    @SerializedName("is_active")
    String mIsActive;
    @SerializedName("is_verify")
    String mIsVerify;
    @SerializedName("avatar")
    String mAvatar;
    @SerializedName("token")
    String mToken;
    @SerializedName("wallet")
    int mWallet;

    public String getAvatar() {
        return mAvatar;
    }

    public String getID() {
        return mID;
    }

    public String getAddress() {
        return mAddress;
    }

    public String getAccountID() {
        return mAccount;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getToken() {
        return mToken;
    }

    public int getWallet() {
        return mWallet;
    }

    public void setWallet(int data) {
        this.mWallet = data;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    public void setIsActive(String isActive) {
        this.mIsActive = isActive;
    }

    public void setIsVerify(String isVerify) {
        this.mIsVerify = isVerify;
    }

    public boolean isActive() {
        if (mIsActive.equalsIgnoreCase("1")) {
            return true;
        }

        return false;
    }

    public boolean isVerify() {
        if (mIsVerify.equalsIgnoreCase("1")) {
            return true;
        }

        return false;
    }
}
