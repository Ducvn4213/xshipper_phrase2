package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DirectionLeg {
    @SerializedName("steps")
    List<DirectionStep> mDirectionStep;

    public DirectionLeg() {}

    public List<DirectionStep> getDirectionStep() {
        return mDirectionStep;
    }
}
