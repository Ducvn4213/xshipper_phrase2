package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class DirectionLocation2 {
    @SerializedName("latitude")
    Double mLat;
    @SerializedName("longitude")
    Double mLon;

    public DirectionLocation2() {}

    public Double getLat() {
        return mLat;
    }

    public Double getLng() {
        return mLon;
    }

    public void setLat(Double data) {
        mLat = data;
    }

    public void setLng(Double data) {
        mLon = data;
    }
}
