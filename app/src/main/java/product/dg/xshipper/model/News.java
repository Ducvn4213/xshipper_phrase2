package product.dg.xshipper.model;

import com.google.gson.annotations.SerializedName;

public class News {
    @SerializedName("id")
    String mID;
    @SerializedName("date")
    String mDate;
    @SerializedName("summary")
    String mSummary;
    @SerializedName("title")
    String mTitle;

    public String getID() {
        return mID;
    }
    public String getDate() {
        return mDate;
    }
    public String getSummary() {
        return mSummary;
    }
    public String getTitle() {
        return mTitle;
    }
}
