package product.dg.xshipper.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import product.dg.xshipper.R;
import product.dg.xshipper.certificate.CertificateActivity;
import product.dg.xshipper.change_password.ChangePasswordActivity;
import product.dg.xshipper.login.LoginActivity;
import product.dg.xshipper.model.User;
import product.dg.xshipper.nganluong.NLService;
import product.dg.xshipper.nganluong.ui.activity.CheckOutActivity;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.util.FS;

public class ProfilePresenter implements ProfileInterfaces.ProvidedPresenterOps, ProfileInterfaces.RequiredPresenterOps {
    private WeakReference<ProfileInterfaces.RequiredViewOps> mView;
    private ProfileInterfaces.ProvidedModelOps mModel;

    ProfilePresenter(ProfileInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ProfileInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void setView(ProfileInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doUpdate(String name, String address, String phone) {
        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_name);
            return;
        }

        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_address);
            return;
        }

        mView.get().showWaiting();
        mModel.doUpdate(name, address, phone);
    }

    @Override
    public void openChangePassword() {
        Intent intent = new Intent(mView.get().getParentActivity(), ChangePasswordActivity.class);
        ActivityCompat.startActivity(mView.get().getParentActivity(), intent, null);
    }

    @Override
    public void openCertificate() {
        Intent intent = new Intent(mView.get().getParentActivity(), CertificateActivity.class);
        ActivityCompat.startActivity(mView.get().getParentActivity(), intent, null);
    }

    @Override
    public void openLogin() {
        Intent intent = new Intent(mView.get().getParentActivity(), LoginActivity.class);
        ActivityCompat.startActivity(mView.get().getParentActivity(), intent, null);
    }

    @Override
    public void addMoneyByATMNganLuong() {
        mView.get().performAddMoneyOption();
    }

    @Override
    public void addMoneyByMobileCard() {
        mView.get().performAddMoneyByMobileCardOption();
    }

    @Override
    public void addMoneyByMobileCardWith(int branch, String number, String seri) {

        if (number.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.add_money_card_number_empty);
            return;
        }

        if (seri.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.add_money_card_seri_empty);
            return;
        }

        mView.get().showWaiting();
        mModel.addMoneyByMobileCardWith(branch, number, seri);
    }

    @Override
    public void addMoneyTypeOption() {
        mView.get().performAddMoneyTypeOption();
    }

    @Override
    public void addMoney(String money) {
        FSService fsService = FSService.getInstance();
        User user = fsService.getCurrentActiveUser();
        final NLService nlService = NLService.getInstance();
        nlService.amountPaid = money;
        nlService.sendOrderObject(mView.get().getParentActivity(), user.getName(), money, user.getEmail(), user.getPhone(), user.getAddress(), new NLService.NLSendOrderCallback() {
            @Override
            public void onSuccess(String tokenCode, String checkoutUrl) {
                try {
                    Intent intent = new Intent(mView.get().getParentActivity(), CheckOutActivity.class);
                    intent.putExtra(CheckOutActivity.TOKEN_CODE, tokenCode);
                    intent.putExtra(CheckOutActivity.CHECKOUT_URL, checkoutUrl);
                    ActivityCompat.startActivityForResult(mView.get().getParentActivity(), intent, NLService.CALLBACK_CODE, null);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    public void addMoneyToAccount(String money) {
        mModel.addMoneyToAccount(money);
    }

    @Override
    public void updateDisplayUserInfo() {
        mModel.displayUserInfo();
    }

    @Override
    public void openImagePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ActivityCompat.startActivityForResult(mView.get().getParentActivity(), Intent.createChooser(intent, "Select Picture"), FS.PICK_IMAGE_REQUEST_CODE, null);
    }

    @Override
    public void doUpdateAvatar() {
        mView.get().showWaiting();
        mModel.doUpdateAvatar();
    }

    @Override
    public void setCurrentAvatarBitmap(Bitmap bitmap) {
        mModel.setCurrentAvatarBitmap(bitmap);
    }

    @Override
    public void updateAvatarSuccess() {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().updateAvatarSuccess();
            }
        });
    }

    @Override
    public void addMoneySuccess() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().addMoneySuccess();
            }
        });
    }

    @Override
    public void addMoneyFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().addMoneyFail();
            }
        });
    }

    @Override
    public void addMoneyByCardFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().addMoneyByCardFail();
            }
        });
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onUpdateSuccess() {
        mView.get().hideWaiting();
        mView.get().updateSuccessCallback();
    }

    @Override
    public void displayUserInfo(final String name, final String email, final String address, final String phone, final String avatar, final int money, final boolean isVerify, final boolean isFacebookLogged) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().displayUserInfo(name, email, address, phone, avatar, money, isVerify, isFacebookLogged);
            }
        });
    }

    @Override
    public void displayUserInfo_None() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().displayUserInfo_None();
            }
        });
    }
}