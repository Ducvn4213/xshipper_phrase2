package product.dg.xshipper.profile;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;

import product.dg.xshipper.model.User;

public class ProfileInterfaces {
    interface RequiredViewOps {
        Activity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateSuccessCallback();
        void performAddMoneyOption();
        void performAddMoneyByMobileCardOption();
        void performAddMoneyTypeOption();

        void addMoneySuccess();
        void addMoneyFail();
        void addMoneyByCardFail();

        void displayUserInfo(String name, String email, String address, String phone, String avatar, int money, boolean isVerify, boolean isFacebookLogged);
        void displayUserInfo_None();
        void updateAvatarSuccess();
    }

    interface ProvidedPresenterOps {
        void setView(RequiredViewOps view);
        void doUpdate(String name, String address, String phone);
        void openChangePassword();
        void openCertificate();
        void openLogin();
        void addMoneyByATMNganLuong();
        void addMoneyByMobileCard();
        void addMoneyByMobileCardWith(int branch, String number, String seri);
        void addMoneyTypeOption();
        void addMoney(String money);
        void addMoneyToAccount(String money);
        void updateDisplayUserInfo();
        void openImagePicker();
        void doUpdateAvatar();
        void setCurrentAvatarBitmap(Bitmap bitmap);

    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);
        void onUpdateSuccess();
        void displayUserInfo(String name, String email, String address, String phone, String avatar, int money, boolean isVerify, boolean isFacebookLogged);
        void displayUserInfo_None();
        void updateAvatarSuccess();

        void addMoneySuccess();
        void addMoneyFail();
        void addMoneyByCardFail();
    }

    interface ProvidedModelOps {
        void doUpdate(String name, String address, String phone);

        void setFacebookUser(User user);
        void setUser(User user);
        void addMoneyToAccount(String money);
        void addMoneyByMobileCardWith(int branch, String number, String seri);
        User getFacebookUser();
        User getuser();

        Boolean isFacebookUser();

        void displayUserInfo();

        void setCurrentAvatarBitmap(Bitmap bitmap);
        void doUpdateAvatar();
    }
}
