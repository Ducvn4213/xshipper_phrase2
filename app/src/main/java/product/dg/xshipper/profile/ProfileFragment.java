package product.dg.xshipper.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.nganluong.NLService;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.util.FS;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements ProfileInterfaces.RequiredViewOps{

    ProgressDialog mProgressDialog;
    private ProfileInterfaces.ProvidedPresenterOps mPresenter;

    EditText mName;
    EditText mEmail;
    EditText mPhone;
    EditText mAddress;
    EditText mWallet;
    Button mCertificate;
    Button mChangePassword;
    Button mLogin;
    TextView mNameView;
    TextView mEmailView;
    ImageButton mChangeImage;
    ImageButton mApplyImage;
    ImageView mAvatar;
    ImageButton mAddMoney;

    private static ProfileFragment instance;

    public static ProfileFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_profile, container, false);

        init();

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    private void init() {
        ProfilePresenter presenter = new ProfilePresenter(this);
        ProfileModel model = new ProfileModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControls(View view) {
        mName = (EditText) view.findViewById(R.id.edt_name);
        mEmail = (EditText) view.findViewById(R.id.edt_email);
        mPhone = (EditText) view.findViewById(R.id.edt_phone);
        mAddress = (EditText) view.findViewById(R.id.edt_address);
        mWallet = (EditText) view.findViewById(R.id.edt_wallet);
        mCertificate = (Button) view.findViewById(R.id.btn_certificate);
        mChangePassword = (Button) view.findViewById(R.id.btn_change_password);
        mLogin = (Button) view.findViewById(R.id.btn_login);
        mNameView = (TextView) view.findViewById(R.id.tv_name);
        mEmailView = (TextView) view.findViewById(R.id.tv_email);
        mChangeImage = (ImageButton) view.findViewById(R.id.ibtn_change_image);
        mAvatar = (ImageView) view.findViewById(R.id.iv_avatar);
        mApplyImage = (ImageButton) view.findViewById(R.id.ibtn_apply_image);
        mAddMoney = (ImageButton) view.findViewById(R.id.btn_add_money);
    }

    private void setupControlEvents() {
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openLogin();
            }
        });

        mCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openCertificate();
            }
        });

        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openChangePassword();
            }
        });

        mChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.openImagePicker();
            }
        });

        mAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoneyTypeOption();
            }
        });

        mApplyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.doUpdateAvatar();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.updateDisplayUserInfo();

        instance = this;
    }

    @Override
    public Activity getParentActivity() {
        return ProfileFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void updateSuccessCallback() {
        mPresenter.updateDisplayUserInfo();
    }



    @Override
    public void performAddMoneyOption() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_add_money_dialog, null);
        dialogBuilder.setView(dialogView);

        Button btn10 = (Button) dialogView.findViewById(R.id.btn_10);
        Button btn20 = (Button) dialogView.findViewById(R.id.btn_20);
        Button btn50 = (Button) dialogView.findViewById(R.id.btn_50);
        Button btn100 = (Button) dialogView.findViewById(R.id.btn_100);
        Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

        final AlertDialog alertDialog = dialogBuilder.create();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoney("10000");
                alertDialog.dismiss();
            }
        });

        btn20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoney("20000");
                alertDialog.dismiss();
            }
        });

        btn50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoney("50000");
                alertDialog.dismiss();
            }
        });

        btn100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoney("100000");
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void performAddMoneyByMobileCardOption() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_add_money_by_mobile_card_dialog, null);
        dialogBuilder.setView(dialogView);

        final AppCompatSpinner spnCardType = (AppCompatSpinner) dialogView.findViewById(R.id.card_type);
        final EditText edtCardNumber = (EditText) dialogView.findViewById(R.id.card_number);
        final EditText edtCardSeri = (EditText) dialogView.findViewById(R.id.card_seri);
        Button btnSubmit = (Button) dialogView.findViewById(R.id.btn_submit);
        Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

        List<String> itemList = new ArrayList<>();
        String[] items = getResources().getStringArray(R.array.array_mobile_card_branch);

        Collections.addAll(itemList, items);

        ArrayAdapter<String> itemAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, R.id.tv_item_text, itemList);
        itemAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item);
        spnCardType.setAdapter(itemAdapter);

        final AlertDialog alertDialog = dialogBuilder.create();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number = edtCardNumber.getText().toString();
                String seri = edtCardSeri.getText().toString();
                int branch = spnCardType.getSelectedItemPosition();

                mPresenter.addMoneyByMobileCardWith(branch, number, seri);
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void performAddMoneyTypeOption() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_add_money_type_dialog, null);
        dialogBuilder.setView(dialogView);

        Button btnATMNganluong = (Button) dialogView.findViewById(R.id.btn_atm_nganluong);
        Button btnMobileCard = (Button) dialogView.findViewById(R.id.btn_mobile_card);

        Button cancel = (Button) dialogView.findViewById(R.id.btn_cancel);

        final AlertDialog alertDialog = dialogBuilder.create();

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        btnATMNganluong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoneyByATMNganLuong();
                alertDialog.dismiss();
            }
        });

        btnMobileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addMoneyByMobileCard();
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void addMoneySuccess() {
        showDialog(R.string.dialog_notice_title, R.string.dialog_message_add_money_success);
        mPresenter.updateDisplayUserInfo();
        NLService.getInstance().amountPaid = "";
    }

    @Override
    public void addMoneyFail() {
        showDialog(R.string.dialog_notice_title, R.string.dialog_message_add_money_fail);
        mPresenter.updateDisplayUserInfo();
        NLService.getInstance().amountPaid = "";
    }

    @Override
    public void addMoneyByCardFail() {
        showDialog(R.string.dialog_notice_title, R.string.dialog_message_add_money_card_fail);
        mPresenter.updateDisplayUserInfo();
        NLService.getInstance().amountPaid = "";
    }

    @Override
    public void displayUserInfo(String name, String email, String address, String phone, String avatar, int money, boolean isVerify, boolean isFacebookLogged) {
        mName.setVisibility(View.VISIBLE);
        mEmail.setVisibility(View.VISIBLE);
        mPhone.setVisibility(View.VISIBLE);
        mAddress.setVisibility(View.VISIBLE);
        mWallet.setVisibility(View.VISIBLE);

        mLogin.setVisibility(View.GONE);
        mChangeImage.setVisibility(View.VISIBLE);

        mName.setText(name);
        mNameView.setText(name);
        mEmail.setText(email);
        mEmailView.setText(email);
        mPhone.setText(phone);
        mAddress.setText(address);
        mWallet.setText(money + "");

        if (avatar != null && !avatar.trim().equalsIgnoreCase("")) {
            Picasso.with(getParentActivity()).invalidate(FS.IMAGE_HOST + avatar);
            Picasso.with(getParentActivity()).load(FS.IMAGE_HOST + avatar).into(mAvatar);
        }

        if (isVerify) {
            mCertificate.setVisibility(View.GONE);
        }
        else {
            mCertificate.setVisibility(View.VISIBLE);
        }

        if (isFacebookLogged) {
            mChangePassword.setVisibility(View.GONE);
        }
        else {
            mChangePassword.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void displayUserInfo_None() {
        displayUserInfo("", "", "", "", "", 0, true, false);

        mName.setVisibility(View.GONE);
        mEmail.setVisibility(View.GONE);
        mPhone.setVisibility(View.GONE);
        mAddress.setVisibility(View.GONE);
        mWallet.setVisibility(View.GONE);
        mCertificate.setVisibility(View.GONE);
        mChangePassword.setVisibility(View.GONE);

        mLogin.setVisibility(View.VISIBLE);
        mChangeImage.setVisibility(View.GONE);

        mAvatar.setImageResource(R.drawable.profile_default);

        mNameView.setText(getString(R.string.profile_name_not_logged));
        mEmailView.setText(getString(R.string.profile_email_not_logged));
    }

    @Override
    public void updateAvatarSuccess() {
        mChangeImage.setVisibility(View.VISIBLE);
        mApplyImage.setVisibility(View.GONE);
        mPresenter.setCurrentAvatarBitmap(null);
    }

    public Boolean isEdit() {
        return mName.isEnabled();
    }

    public void enableEdit() {
        FSService service = FSService.getInstance();

        mPhone.setEnabled(true);
        mAddress.setEnabled(true);

        if (service.getCurrentFacebookUser() == null) {
            mName.setEnabled(true);
            mName.requestFocus();
        }
        else {
            mName.setEnabled(false);
            mPhone.requestFocus();
        }

        mChangePassword.setVisibility(View.GONE);
        mCertificate.setVisibility(View.GONE);
    }

    public void disableEdit() {
        mName.setEnabled(false);
        mPhone.setEnabled(false);
        mAddress.setEnabled(false);

        mName.requestFocus();
        mChangePassword.setVisibility(View.VISIBLE);
    }

    public void doUpdateProfileInfo() {
        disableEdit();
        String name = mName.getText().toString();
        String phone = mPhone.getText().toString();
        String address = mAddress.getText().toString();

        mPresenter.doUpdate(name, address, phone);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FS.PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
            changeImage(filePath);
        }

        if (requestCode == NLService.CALLBACK_CODE) {
            NLService nlService = NLService.getInstance();
            if (nlService.isPaid) {
                mPresenter.addMoneyToAccount(nlService.amountPaid);
            }
            else {
                showDialog(R.string.dialog_notice_title, R.string.dialog_message_add_money_fail);
            }
        }
    }

    private void changeImage(Uri filePath) {
        try {
            mChangeImage.setVisibility(View.GONE);
            mApplyImage.setVisibility(View.VISIBLE);
            Picasso.with(getParentActivity()).load(filePath).into(mAvatar);

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
            mPresenter.setCurrentAvatarBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
