package product.dg.xshipper.get_direction;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.model.Direction;
import product.dg.xshipper.model.DirectionLocation;
import product.dg.xshipper.model.DirectionStep;
import product.dg.xshipper.model.Distance;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketWithDirection;
import product.dg.xshipper.model.SnappedResponse;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class GetDirectionModel implements GetDirectionInterfaces.ProvidedModelOps {
    private GetDirectionInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();
    private List<GigTicketWithDirection> results = new ArrayList<>();

    GetDirectionModel(GetDirectionInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loadData(final LatLng currentLocation) {
        GigTicketWithDirection gigTicketWithDirection = new GigTicketWithDirection();
        gigTicketWithDirection.setLocation(currentLocation);
        gigTicketWithDirection.setGigTicket(null);
        results.add(gigTicketWithDirection);

        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "3"));

        mService.get_book_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                getDirection(data.getData());
            }

            @Override
            public void onFail(String error) {
                mPresenter.presentData(null);
            }
        });
    }

    @Override
    public void drawEndPoints() {
        mPresenter.presentEndPoints(results);
    }

    void getDirection(final List<GigTicket> data) {
        if (data.size() == 1) {
            mService.getLocationFromAddress(data.get(0).getTo(), new FSService.Callback<DirectionLocation>() {
                @Override
                public void onSuccess(DirectionLocation _data) {
                    GigTicketWithDirection gigTicketWithDirection = new GigTicketWithDirection();
                    gigTicketWithDirection.setGigTicket(data.get(0));

                    LatLng latLng = new LatLng(_data.getLat(), _data.getLng());
                    gigTicketWithDirection.setLocation(latLng);

                    results.add(gigTicketWithDirection);
                    getToTalDirection(results);
                }

                @Override
                public void onFail(String error) {
                    mPresenter.presentData(null);
                }
            });
            return;
        }
        final List<GigTicket> _data = data;
        final GigTicketWithDirection _fromPoint = results.get(results.size() - 1);

        List<String> locationList = new ArrayList<>();
        for (GigTicket gi : _data) {
            locationList.add(gi.getTo());
        }

        mService.getDistance2(_fromPoint.getLocation().latitude + "", _fromPoint.getLocation().longitude + "", locationList, new FSService.Callback<Distance>() {
            @Override
            public void onSuccess(Distance data) {
                GigTicket getValue = _data.get(data.getIndex());
                LatLng latLng = new LatLng(data.getLatitude(), data.getLongitude());

                GigTicketWithDirection gigTicketWithDirection = new GigTicketWithDirection();
                gigTicketWithDirection.setGigTicket(getValue);
                gigTicketWithDirection.setLocation(latLng);

                results.add(gigTicketWithDirection);

                _data.remove(data.getIndex());
                getDirection(_data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.presentData(null);
            }
        });
    }

    List<DirectionLocation> mDataToShow = new ArrayList<>();
    void getToTalDirection(List<GigTicketWithDirection> data) {
        getDirectionOf(0, data);
    }

    void getDirectionOf(final int index, final List<GigTicketWithDirection> data) {
        if (index == (data.size() - 1)) {
            mService.getSnapRoad(mDataToShow, new FSService.Callback<SnappedResponse>() {
                @Override
                public void onSuccess(SnappedResponse data) {
                    mPresenter.presentData(data.getSnappedPoints());
                }

                @Override
                public void onFail(String error) {
                    mPresenter.presentData(null);
                }
            });

            return;
        }

        GigTicketWithDirection from = data.get(index);
        GigTicketWithDirection to = data.get(index + 1);

        DirectionLocation _from = new DirectionLocation();
        _from.setLat(from.getLocation().latitude);
        _from.setLng(from.getLocation().longitude);

        DirectionLocation _to = new DirectionLocation();
        _to.setLat(to.getLocation().latitude);
        _to.setLng(to.getLocation().longitude);

        mService.getDirection(_from, _to, new FSService.Callback<Direction>() {
            @Override
            public void onSuccess(Direction _data) {
                List<DirectionStep> steps = _data.getDirectionRoutes().get(0).getDirectionLegs().get(0).getDirectionStep();

                for (DirectionStep step : steps) {
                    List<LatLng> points = step.getPolyline().getPoints();
                    for (LatLng point : points) {
                        DirectionLocation directionLocation = new DirectionLocation();
                        directionLocation.setLat(point.latitude);
                        directionLocation.setLng(point.longitude);
                        mDataToShow.add(directionLocation);
                    }
                }

                int newIndex = index + 1;
                getDirectionOf(newIndex, data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.presentData(null);
            }
        });
    }
}
