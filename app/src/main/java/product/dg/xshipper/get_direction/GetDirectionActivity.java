package product.dg.xshipper.get_direction;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.DirectionLocation;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketWithDirection;
import product.dg.xshipper.model.SnappedPoint;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.TrackerGPS;

public class GetDirectionActivity extends AppCompatActivity implements OnMapReadyCallback, GetDirectionInterfaces.RequiredViewOps {

    private GoogleMap mMap;
    private ProgressDialog mProgressDialog;
    SupportMapFragment mMapFragment;
    GetDirectionPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_direction);

        bindingControls();
        setupControlEvents();

        init();
    }

    void bindingControls() {
        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.f_map);
    }

    void setupControlEvents() {}

    void init() {
        mMapFragment.getMapAsync(this);

        GetDirectionPresenter presenter = new GetDirectionPresenter(this);
        GetDirectionModel model = new GetDirectionModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        LatLng defaultLocation = getCurrentLocationLatLng();
        mMap.addMarker(new MarkerOptions().position(defaultLocation).title(getString(R.string.get_current_location_your_location)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(defaultLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

        showWaiting();
        mPresenter.loadData(defaultLocation);
    }

    private LatLng getCurrentLocationLatLng() {
        LatLng defaultLocation = new LatLng(0, 0);
        TrackerGPS trackerGPS = new TrackerGPS(GetDirectionActivity.this);
        if (trackerGPS.canGetLocation()) {
            if (trackerGPS.getLatitude() == 0 && trackerGPS.getLongitude() == 0) {
                return getCurrentLocationLatLng();
            }
            defaultLocation = new LatLng(trackerGPS.getLatitude(), trackerGPS.getLongitude());
            return defaultLocation;
        }

        return defaultLocation;
    }

    @Override
    public AppCompatActivity getActivity() {
        return GetDirectionActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(GetDirectionActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(GetDirectionActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(GetDirectionActivity.this, getString(R.string.dialog_message_getting_direction), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void presentData(List<SnappedPoint> data) {
        if (data == null) {
            final AlertDialog dialog = new AlertDialog.Builder(GetDirectionActivity.this)
                    .setTitle(getString(R.string.dialog_title_error))
                    .setMessage(R.string.dialog_no_ticket_to_get_direction)
                    .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            GetDirectionActivity.this.finish();
                        }
                    }).create();
            if (!dialog.isShowing()) {
                dialog.show();
            }
            return;
        }

        mPresenter.drawEndPoints();
        hideWaiting();
        PolylineOptions options = new PolylineOptions().width(10).color(Color.BLUE);

        for (SnappedPoint d : data) {
            LatLng point = new LatLng(d.getDirectionLocation2().getLat(), d.getDirectionLocation2().getLng());
            options.add(point);
        }

        mMap.addPolyline(options);
    }

    @Override
    public void presentEndPoints(List<GigTicketWithDirection> data) {
        data.remove(0);
        for (GigTicketWithDirection gtwd : data) {
            LatLng latLng = gtwd.getLocation();
            MarkerOptions options = new MarkerOptions();
            BitmapDescriptor icon;
            if (gtwd.getGigTicket().getSize().equalsIgnoreCase("nhỏ")) {
                icon = BitmapDescriptorFactory.fromResource(R.mipmap.gig_small);
            }
            else {
                icon = BitmapDescriptorFactory.fromResource(R.mipmap.gig_medium);
            }

            options.flat(true);
            options.icon(icon);
            options.position(latLng);
            options.title(gtwd.getGigTicket().getTitle());

            mMap.addMarker(options);
        }

        hideWaiting();
    }


}
