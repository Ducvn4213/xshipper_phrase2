package product.dg.xshipper.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import product.dg.xshipper.R;
import product.dg.xshipper.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity implements LoginInterfaces.RequiredViewOps {

    TextView mRegister;
    Button mLogin;
    EditText mUsername;
    EditText mPassword;
    CallbackManager mCallbackManager;
    ProgressDialog mProgressDialog;

    private LoginInterfaces.ProvidedPresenterOps mPresenter;

    private static LoginActivity instance;
    public static LoginActivity getInstance() {
        return instance;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        init();

        bindingControl();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        instance = this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        instance = null;
    }

    private void init() {
        LoginPresenter presenter = new LoginPresenter(LoginActivity.this);
        LoginModel model = new LoginModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    private void bindingControl() {
        mRegister = (TextView) findViewById(R.id.tv_register);
        mLogin = (Button) findViewById(R.id.btn_login);
        mUsername = (EditText) findViewById(R.id.edt_username);
        mPassword = (EditText) findViewById(R.id.edt_password);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setupControlEvents() {
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                LoginActivity.this.finish();
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();

                mPresenter.doLogin(username, password);
            }
        });
    }

    @Override
    public AppCompatActivity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void loginSuccessCallback() {
        mPresenter.openMain();
    }

    AlertDialog wrongVersionDialog;
    public void versionFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (wrongVersionDialog == null) {
                    wrongVersionDialog = new AlertDialog.Builder(LoginActivity.this)
                            .setTitle(getString(R.string.dialog_notice_title))
                            .setMessage(getString(R.string.dialog_message_version_fail))
                            .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                    LoginActivity.this.finish();
                                }
                            })
                            .create();
                }
                if (!wrongVersionDialog.isShowing()) {
                    wrongVersionDialog.show();
                }
            }
        });
    }
}
