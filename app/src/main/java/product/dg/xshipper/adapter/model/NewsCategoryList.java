package product.dg.xshipper.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipper.model.NewsCategory;


public class NewsCategoryList {
    @SerializedName("data")
    List<NewsCategory> mData;

    public NewsCategoryList() {}

    public List<NewsCategory> getData() {
        return mData;
    }
}
