package product.dg.xshipper.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipper.model.News;

public class NewsList {
    @SerializedName("data")
    List<News> mData;

    public NewsList() {}

    public List<News> getData() {
        return mData;
    }
}
