package product.dg.xshipper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.News;


public class NewsAdapter extends ArrayAdapter<News> {

    private Context mContext;
    private List<News> mData;
    private static LayoutInflater mInflater = null;

    public NewsAdapter(Context context, List<News> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_news_item, null);
        }

        TextView title = (TextView) view.findViewById(R.id.tv_title);
        TextView date = (TextView) view.findViewById(R.id.tv_date);
        TextView sum = (TextView) view.findViewById(R.id.tv_summary);

        News data = mData.get(position);

        title.setText(data.getTitle());
        date.setText(data.getDate());
        sum.setText(data.getSummary());

        return view;
    }
}
