package product.dg.xshipper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.model.GigTicketGroup;
import product.dg.xshipper.model.PresentGigTicket;
import product.dg.xshipper.response.GroupGigTicket;

public class PresentGigTicketAdapter extends ArrayAdapter<PresentGigTicket> {

    private Context mContext;
    private List<PresentGigTicket> mData;
    private static LayoutInflater mInflater = null;

    public PresentGigTicketAdapter(Context context, List<PresentGigTicket> data) {
        super(context, R.layout.layout_manage_gig_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_manage_gig_item, null);
        }


        TextView title = (TextView) view.findViewById(R.id.edt_name);
        TextView size = (TextView) view.findViewById(R.id.edt_size);
        TextView from = (TextView) view.findViewById(R.id.edt_from);
        TextView to = (TextView) view.findViewById(R.id.edt_to);

        PresentGigTicket data = mData.get(position);

        if (data.getIsGroup()) {
            GigTicketGroup _data = data.getGigTicketGroup();

            title.setText(_data.getTitle());
            size.setText(_data.getSize());
            from.setText("From: " + _data.getFrom());
            to.setText("To: " + _data.getTo());
        }
        else {
            GigTicket _data = data.getGigTicket();

            title.setText(_data.getTitle());
            size.setText(_data.getSize());
            from.setText("From: " + _data.getFrom());
            to.setText("To: " + _data.getTo());
        }

        return view;
    }
}
