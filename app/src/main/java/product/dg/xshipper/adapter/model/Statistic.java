package product.dg.xshipper.adapter.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import product.dg.xshipper.model.StatisticCount;
import product.dg.xshipper.model.StatisticIncome;


public class Statistic {
    @SerializedName("count")
    List<StatisticCount> mDataCount;
    @SerializedName("income")
    List<StatisticIncome> mDataIncome;

    public Statistic() {}

    public List<StatisticCount> getDataCount() {
        return mDataCount;
    }
    public List<StatisticIncome> getDataIncome() {
        return mDataIncome;
    }
}
