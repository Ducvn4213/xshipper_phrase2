package product.dg.xshipper.register;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;

import product.dg.xshipper.R;
import product.dg.xshipper.certificate.CertificateActivity;
import product.dg.xshipper.util.Utils;

public class RegisterPresenter implements RegisterInterfaces.ProvidedPresenterOps, RegisterInterfaces.RequiredPresenterOps {
    private WeakReference<RegisterInterfaces.RequiredViewOps> mView;
    private RegisterInterfaces.ProvidedModelOps mModel;

    RegisterPresenter(RegisterInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void setView(RegisterInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    @Override
    public void doRegister(String email, String name, String address, String phone, String password) {
        if (address.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_address);
            return;
        }

        if (password.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_password);
            return;
        }

        if (name.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_name);
            return;
        }

        if (!Utils.isValidEmail(email.trim())) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_email_invalid);
            return;
        }

        if (email.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_email);
            return;
        }

        if (password.trim().length() < 6) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_password_too_short);
            return;
        }

        if (phone.trim().equalsIgnoreCase("")) {
            mView.get().showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_phone);
            return;
        }

        mView.get().showWaiting();
        mModel.doRegister(email, name, address, phone, password);
    }

    @Override
    public void moveToVerify() {
        Intent intent = new Intent(mView.get().getActivity(), CertificateActivity.class);
        ActivityCompat.startActivity(mView.get().getActivity(), intent, null);
        mView.get().getActivity().finish();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().hideWaiting();
        mView.get().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void onRegisterSuccess() {
        mView.get().hideWaiting();
        mView.get().registerSuccessCallback();
    }

    public void setModel(RegisterInterfaces.ProvidedModelOps model) {
        mModel = model;
    }
}