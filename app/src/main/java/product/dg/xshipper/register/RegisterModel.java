package product.dg.xshipper.register;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.User;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class RegisterModel implements RegisterInterfaces.ProvidedModelOps {
    private RegisterInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    RegisterModel(RegisterInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }
    @Override
    public void doRegister(String email, String name, String address, String phone, String password) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_email", email));
        paramList.add(new Param("_name", name));
        paramList.add(new Param("_address", address));
        paramList.add(new Param("_password", password));
        paramList.add(new Param("_phone", phone));

        mService.register(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                mService.setCurrentUser(data);
                mPresenter.onRegisterSuccess();
                doUpdateRegistrationID();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    void doUpdateRegistrationID() {
        List<Param> paramList = new ArrayList<>();
        mService.updateRegistrationID(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                //TODO
            }

            @Override
            public void onFail(String error) {
                //TODO
            }
        });
    }
}
