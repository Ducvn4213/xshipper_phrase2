package product.dg.xshipper.promotion;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import product.dg.xshipper.R;


public class PromotionFragment extends Fragment implements PromotionInterfaces.RequiredViewOps  {
    ProgressDialog mProgressDialog;

    EditText mCode;
    Button mSubmit;

    private PromotionInterfaces.ProvidedPresenterOps mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_promotion_code, container, false);

        init();

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    private void init() {
        PromotionPresenter presenter = new PromotionPresenter(PromotionFragment.this);
        PromotionModel model = new PromotionModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    void bindingControls(View view) {
        mCode = (EditText) view.findViewById(R.id.edt_code);
        mSubmit = (Button) view.findViewById(R.id.btn_submit);
    }

    void setupControlEvents() {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.submitCode(mCode.getText().toString());
            }
        });
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) PromotionFragment.this.getActivity();
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void promotionResultCallback() {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(R.string.dialog_notice_title))
                .setMessage(getString(R.string.promotion_code_success))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mCode.setText("");
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
