package product.dg.xshipper.promotion;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.model.User;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class PromotionModel implements PromotionInterfaces.ProvidedModelOps {
    private PromotionInterfaces.RequiredPresenterOps mPresenter;

    private FSService mService = FSService.getInstance();

    PromotionModel(PromotionInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;
    }

    @Override
    public void submitCode(String code) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_code", code));

        mService.submitPromotionCode(paramList, new FSService.Callback<User>() {
            @Override
            public void onSuccess(User data) {
                if (mService.getCurrentFacebookUser() != null) {
                    User faceUser = mService.getCurrentFacebookUser();
                    data.setEmail(faceUser.getEmail());
                    mService.setCurrentFacebookUser(data);
                }
                else {
                    mService.setCurrentUser(data);
                }
                mPresenter.submitPromotionCodeSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, R.string.promotion_code_fail);
            }
        });
    }
}
