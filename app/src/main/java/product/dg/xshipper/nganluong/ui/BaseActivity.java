package product.dg.xshipper.nganluong.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

/**
 * Created by duc.nv on 2/15/2017.
 */

public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }
}