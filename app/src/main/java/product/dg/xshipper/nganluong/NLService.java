package product.dg.xshipper.nganluong;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.rey.material.app.Dialog;
import com.rey.material.widget.Button;

import org.json.JSONObject;

import product.dg.xshipper.R;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.nganluong.api.CheckOrderRequest;
import product.dg.xshipper.nganluong.api.SendOrderRequest;
import product.dg.xshipper.nganluong.bean.CheckOrderBean;
import product.dg.xshipper.nganluong.bean.SendOrderBean;
import product.dg.xshipper.nganluong.ui.activity.CheckOutActivity;
import product.dg.xshipper.nganluong.ui.activity.MainActivity;
import product.dg.xshipper.nganluong.utils.Commons;
import product.dg.xshipper.nganluong.utils.Constant;
import product.dg.xshipper.service.FSService;

public class NLService {
    public static int CALLBACK_CODE = 168;
    public interface NLCallback {
        void onSuccess();
        void onFail();
    }

    public interface NLSendOrderCallback {
        void onSuccess(String tokenCode, String checkoutUrl);
    }
    private static NLService instance;
    private NLService() {}

    public static NLService getInstance() {
        if (instance == null) {
            instance = new NLService();
        }

        return instance;
    }

    public boolean isPaid = false;
    public String amountPaid = "";
    public GigTicket dataProcess = null;

    public void sendOrderObject(final Context context, String fullName, String amount, String email, String phoneNumber, String address, final NLSendOrderCallback callback) {
        SendOrderBean sendOrderBean = new SendOrderBean();
        sendOrderBean.setFunc("sendOrder");
        sendOrderBean.setVersion("1.0");
        sendOrderBean.setMerchantID(Constant.MERCHANT_ID);
        //sendOrderBean.setMerchantAccount(FSService.getInstance().getCurrentActiveUser().getEmail());
        sendOrderBean.setMerchantAccount("eric_hoang@live.com");
        sendOrderBean.setOrderCode(FSService.getInstance().getCurrentActiveUser().getID() + "timshipper");
        sendOrderBean.setTotalAmount(Integer.valueOf(amount));
        sendOrderBean.setCurrency("vnd");
        sendOrderBean.setLanguage("vi");
        sendOrderBean.setReturnUrl(Constant.RETURN_URL);
        sendOrderBean.setCancelUrl(Constant.CANCEL_URL);
        sendOrderBean.setNotifyUrl(Constant.NOTIFY_URL);
        sendOrderBean.setBuyerFullName(fullName);
        sendOrderBean.setBuyerEmail(email);
        sendOrderBean.setBuyerMobile(phoneNumber);
        sendOrderBean.setBuyerAddress(address);

        String checksum = getChecksum(sendOrderBean);
        sendOrderBean.setChecksum(checksum);

        SendOrderRequest sendOrderRequest = new SendOrderRequest();
        sendOrderRequest.execute(context, sendOrderBean);
        sendOrderRequest.getSendOrderRequestOnResult(new SendOrderRequest.SendOrderRequestOnResult() {
            @Override
            public void onSendOrderRequestOnResult(boolean result, String data) {
                if (result == true) {
                    try {
                        JSONObject objResult = new JSONObject(data);
                        String responseCode = objResult.getString("response_code");
                        if (responseCode.equalsIgnoreCase("00")) {
                            String tokenCode = objResult.getString("token_code");
                            String checkoutUrl = objResult.getString("checkout_url");

                            callback.onSuccess(tokenCode, checkoutUrl);
                        } else {
                            showErrorDialog(context, Commons.getCodeError(context, responseCode), false);
                        }
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    }
                }
            }
        });
    }

    private String getChecksum(SendOrderBean sendOrderBean) {
        String stringSendOrder = sendOrderBean.getFunc() + "|" +
                sendOrderBean.getVersion() + "|" +
                sendOrderBean.getMerchantID() + "|" +
                sendOrderBean.getMerchantAccount() + "|" +
                sendOrderBean.getOrderCode() + "|" +
                sendOrderBean.getTotalAmount() + "|" +
                sendOrderBean.getCurrency() + "|" +
                sendOrderBean.getLanguage() + "|" +
                sendOrderBean.getReturnUrl() + "|" +
                sendOrderBean.getCancelUrl() + "|" +
                sendOrderBean.getNotifyUrl() + "|" +
                sendOrderBean.getBuyerFullName() + "|" +
                sendOrderBean.getBuyerEmail() + "|" +
                sendOrderBean.getBuyerMobile() + "|" +
                sendOrderBean.getBuyerAddress() + "|" +
                Constant.MERCHANT_PASSWORD;
        String checksum = Commons.md5(stringSendOrder);

        return checksum;
    }

    private String getChecksum(CheckOrderBean checkOrderBean) {
        String stringSendOrder = checkOrderBean.getFunc() + "|" +
                checkOrderBean.getVersion() + "|" +
                checkOrderBean.getMerchantID() + "|" +
                checkOrderBean.getTokenCode() + "|" +
                Constant.MERCHANT_PASSWORD;
        String checksum = Commons.md5(stringSendOrder);

        return checksum;
    }

    public void checkOrderObject(final Context context, String mTokenCode, final NLCallback callback) {
        CheckOrderBean checkOrderBean = new CheckOrderBean();
        checkOrderBean.setFunc("checkOrder");
        checkOrderBean.setVersion("1.0");
        checkOrderBean.setMerchantID(Constant.MERCHANT_ID);
        checkOrderBean.setTokenCode(mTokenCode);

        String checksum = getChecksum(checkOrderBean);
        checkOrderBean.setChecksum(checksum);

        CheckOrderRequest checkOrderRequest = new CheckOrderRequest();
        checkOrderRequest.execute(context, checkOrderBean);
        checkOrderRequest.getCheckOrderRequestOnResult(new CheckOrderRequest.CheckOrderRequestOnResult() {
            @Override
            public void onCheckOrderRequestOnResult(boolean result, String data) {
                if (result == true) {
                    try {
                        JSONObject objResult = new JSONObject(data);
                        String responseCode = objResult.getString("response_code");
                        if (responseCode.equalsIgnoreCase("00")) {
                            callback.onSuccess();
                        } else {
                            showErrorDialog(context, Commons.getCodeError(context, responseCode), false);
                            callback.onFail();
                        }
                    } catch (Exception ex) {
                        ex.fillInStackTrace();
                    }
                }
            }
        });
    }

    private void showErrorDialog(Context context, String message, final boolean isExit) {
        final Dialog mSuccessDialog = new Dialog(context);
        mSuccessDialog.setContentView(R.layout.nganluong_dialog_success);
        mSuccessDialog.setCancelable(false);
        mSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mSuccessDialog.getWindow().setGravity(Gravity.CENTER);

        TextView txtContent = (TextView) mSuccessDialog.findViewById(R.id.dialog_success_txtContent);
        txtContent.setText(message);
        Button btnClose = (Button) mSuccessDialog.findViewById(R.id.dialog_success_btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSuccessDialog.dismiss();
            }
        });

        mSuccessDialog.show();
    }
}
