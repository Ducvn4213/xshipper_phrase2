package product.dg.xshipper.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.maps.model.LatLng;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Utils {
    public static void hideKeyboard(@NonNull Activity activity) {// Check if no view has focus:
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static void saveValue(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public final static String getValue(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String value = preferences.getString(key, "");
        return value;
    }

    private static String bitMapToString(Bitmap bitmap){
        if (bitmap == null) {
            return "null";
        }
        Bitmap scaleBitmap = bitmap.createScaledBitmap(bitmap, 1000, 1333, true);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        scaleBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte [] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public interface ConvertBitmapToStringCallback {
        void onSuccess(String bitmapString);
        void onFail();
    }

    public static class ConvertBitmapToStringTask extends AsyncTask<String, Void, String> {

        private Bitmap mBitmap;
        private ConvertBitmapToStringCallback mCallback;

        public void setBitmap(Bitmap bitmap) {
            this.mBitmap = bitmap;
        }

        public void setCallback(ConvertBitmapToStringCallback callback) {
            this.mCallback = callback;
        }

        protected String doInBackground(String... bitmap) {
            return bitMapToString(this.mBitmap);
        }

        protected void onPostExecute(String result) {
            if (mCallback != null) {
                mCallback.onSuccess(result);
            }
        }
    }

    public static String getStringAddressFromAddress(Address address) {
        String addressString = "";
        int maxAddressLineIndex = address.getMaxAddressLineIndex();
        for (int i = 0; i <= maxAddressLineIndex; i++) {
            addressString += address.getAddressLine(i) + ", ";
        }
        addressString = addressString.substring(0, addressString.length()-2);
        return addressString;
    }

    public static DateTime incrementDateByOne(DateTime date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date.toDate());
        c.add(Calendar.DATE, 1);
        Date nextDate = c.getTime();
        DateTime newDateTime = new DateTime();

        Long milliseconds = nextDate.getTime();
        Long ticks = milliseconds * 10000;
        newDateTime.withMillis(ticks);
        return newDateTime;
    }

    public static int getPositionByName(List<String> data, String name) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).equalsIgnoreCase(name)) {
                return i;
            }
        }

        return 0;
    }

    public static double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = FS.RADIUS_OF_EARTH;
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        //double km = valueResult / 1;
        //DecimalFormat newFormat = new DecimalFormat("####");
        //int kmInDec = Integer.valueOf(newFormat.format(km));
        //double meter = valueResult % 1000;
        //int meterInDec = Integer.valueOf(newFormat.format(meter));

        return Radius * c;
    }

    public static List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}
