package product.dg.xshipper.util;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import product.dg.xshipper.MainActivity;
import product.dg.xshipper.R;

import static product.dg.xshipper.service.FSService.isPerformNoLocationActivity;

public class NoLocationServiceActivity extends AppCompatActivity {

    Button okButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_gps_service);

        bindingControls();
        initControlEvents();
    }

    void bindingControls() {
        okButton = (Button) findViewById(R.id.btn_ok);
    }

    void initControlEvents() {
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSettingsWindow();
            }
        });
    }

    void showSettingsWindow() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        NoLocationServiceActivity.this.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPerformNoLocationActivity = true;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean checkGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean checkNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //if the location is on again. Close this activity.

        if (checkGPS || checkNetwork) {
            NoLocationServiceActivity.this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        //Do nothing
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isPerformNoLocationActivity = false;
    }
}
