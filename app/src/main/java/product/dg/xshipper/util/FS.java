package product.dg.xshipper.util;

public class FS {
    public static final String APP_VERSION = "1";
    public static final int FACEBOOK_SMS_REQUEST_CODE = 99;
    public static final String DATE_PICKER_FROM_DIALOG_TAG = "fromDatePicker";
    public static final String DATE_PICKER_TO_DIALOG_TAG = "toDatePicker";
    public static final int PICK_IMAGE_REQUEST_CODE = 40;
    public static final int PICK_IMAGE_FRONT_REQUEST_CODE = 41;
    public static final int PICK_IMAGE_BACK_REQUEST_CODE = 42;
    public static final int PICK_CMND_REQUEST_CODE = 43;
    public static final int PICK_GPLX_REQUEST_CODE = 44;
    public static final int PICK_CNHK_REQUEST_CODE = 45;
    public static final int PICK_BLNT_REQUEST_CODE = 46;
    public static final int PICK_IMAGE_COMFIRM_REQUEST_CODE = 47;

    public static final int RADIUS_OF_EARTH = 6371;

    public static final String CARD_TYPE = "CARD_TYPE";
    public static final String CARD_CMND = "CARD_CMND";
    public static final String CARD_GPLX = "CARD_GPLX";

    public static final String CERTIFICATE_TYPE = "CERT_TYPE";
    public static final String CERTIFICATE_CNHK = "CERT_CNHK";
    public static final String CERTIFICATE_BLNT = "CERT_BLNT";

    public static final int PERMISSIONS_REQUEST = 80;
    public static final String SP_USERNAME_KEY = "username";
    public static final String SP_PASSWORD_KEY = "password";

    public static final String WEB_API = "http://timshipper.top/api/api2.php";
    public static final String WEB_API_GET_DATA = "http://timshipper.top/api/getgeodata.php";
    public static final String IMAGE_HOST = "http://timshipper.top/api/";

    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";
    public static final String EXTRA_STATISTIC = "product.dg.xshipping.EXTRA_STATISTIC";

    public static final String EXTRA_MANAGE_GIG = "product.dg.xshipper.EXTRA_MANAGE_GIG";

    public static final String GIG_BOOK_CONFIRM = "gig_book_confirm";
    public static final String GIG_PICK_CONFIRM = "gig_pick_confirm";
    public static final String GIG_PICK = "gig_pick";
    public static final String GIG_PROCESSING = "gig_processing";
    public static final String GIG_COMPLETED = "gig_completed";
    public static final String GIG_WAITING_PICK = "gig_waiting_pick";
    public static final String GIG_DONE = "gig_processing";
    public static final String GIG_DONE_COD = "gig_processing";
    public static final String GIG_ERROR = "gig_processing";
    public static final String STATISTIC_COUNT = "statistic_count";
    public static final String STATISTIC_INCOME = "statistic_income";

    public static final int TIMER_GPS = 5000;

}
