package product.dg.xshipper.statistic;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.model.Statistic;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;
import product.dg.xshipper.statistic.statistic_detail.StatisticDetailActivity;
import product.dg.xshipper.util.FS;

public class StatisticFragment extends Fragment implements CalendarDatePickerDialogFragment.OnDateSetListener{

    private DateTimeFormatter mFormatter;
    ImageButton mFromPicker;
    ImageButton mToPicker;
    Button mSubmit;
    TextView mFrom;
    TextView mTo;
    ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        mFromPicker = (ImageButton) view.findViewById(R.id.btn_from_choosen);
        mToPicker = (ImageButton) view.findViewById(R.id.btn_to_choosen);
        mSubmit = (Button) view.findViewById(R.id.btn_submit);
        mFrom = (TextView) view.findViewById(R.id.tv_from_input);
        mTo = (TextView) view.findViewById(R.id.tv_to_input);
    }

    void setupControlEvents() {
        mFromPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar start = Calendar.getInstance();
                DateTime date = (new DateTime()).withTime(0, 0, 0, 0);
                start.setTimeInMillis(date.getMillis());

                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(StatisticFragment.this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH))
                        .setDoneText(getString(R.string.dialog_button_ok))
                        .setCancelText(getString(R.string.dialog_button_cancel))
                        .setThemeLight();
                cdp.show(StatisticFragment.this.getFragmentManager(), FS.DATE_PICKER_FROM_DIALOG_TAG);
            }
        });

        mToPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar start = Calendar.getInstance();
                DateTime date = (new DateTime()).withTime(0, 0, 0, 0);
                start.setTimeInMillis(date.getMillis());

                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(StatisticFragment.this)
                        .setFirstDayOfWeek(Calendar.MONDAY)
                        .setPreselectedDate(start.get(Calendar.YEAR), start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH))
                        .setDoneText(getString(R.string.dialog_button_ok))
                        .setCancelText(getString(R.string.dialog_button_cancel))
                        .setThemeLight();
                cdp.show(StatisticFragment.this.getFragmentManager(), FS.DATE_PICKER_TO_DIALOG_TAG);
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String from = mFrom.getText().toString();
                String to = mTo.getText().toString();
                String newFrom = correctFormat(from);
                String newTo = correctFormat(to);

                List<Param> params = new ArrayList<>();
                params.add(new Param("_from", newFrom));
                params.add(new Param("_to", newTo));

                FSService service = FSService.getInstance();
                service.loadStatistic(params, new FSService.Callback<Statistic>() {
                    @Override
                    public void onSuccess(Statistic data) {
                        StatisticFragment.this.getParentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getParentActivity(), StatisticDetailActivity.class);
                                intent.putExtra("from", mFrom.getText().toString());
                                intent.putExtra("to", mTo.getText().toString());
                                startActivity(intent);
                            }
                        });
                    }

                    @Override
                    public void onFail(String error) {
                        StatisticFragment.this.getParentActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showDialog(R.string.dialog_title_error, "Không có dữ liệu để thống kê");
                            }
                        });
                    }
                });
            }
        });
    }

    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    String correctFormat(String data) {
        String[] dateSplitted = data.split("-");
        return dateSplitted[2] + "-" + dateSplitted[1] + "-" + dateSplitted[0];
    }

    void init() {
        mFormatter = DateTimeFormat.forPattern(getString(R.string.date_pattern));
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDate = sdf.format(new Date());

        mFrom.setText(currentDate);
        mTo.setText(currentDate);
    }

    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) StatisticFragment.this.getActivity();
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        DateTime dateTime = (new DateTime()).withTime(0, 0, 0, 0);
        dateTime = dateTime.withDate(year, monthOfYear + 1, dayOfMonth);
        if (dialog.getTag().equalsIgnoreCase(FS.DATE_PICKER_FROM_DIALOG_TAG)) {
            mFrom.setText(mFormatter.print(dateTime.getMillis()));
        }
        else {
            mTo.setText(mFormatter.print(dateTime.getMillis()));
        }
    }
}
