package product.dg.xshipper.statistic.statistic_detail;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipper.R;
import product.dg.xshipper.statistic.statistic_detail_count.StatisticCountFragment;
import product.dg.xshipper.statistic.statistic_detail_income.StatisticIncomeFragment;
import product.dg.xshipper.util.FS;


public class StatisticDetailActivity extends AppCompatActivity implements StatisticDetailInterfaces.RequiredViewOps, ViewPager.OnPageChangeListener {

    ProgressDialog mProgressDialog;
    SmartTabLayout mTab;
    ViewPager mViewPager;

    FragmentPagerItemAdapter mAdapter;
    private StatisticDetailInterfaces.ProvidedPresenterOps mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mTab = (SmartTabLayout) findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) findViewById(R.id.vp_container);
    }

    @Override
    protected void onResume() {
        super.onResume();

        String from = getIntent().getStringExtra("from");
        String to = getIntent().getStringExtra("to");

        mPresenter.loadData(from, to);
    }

    void setupControlEvents() {}

    void init() {
        StatisticDetailPresenter presenter = new StatisticDetailPresenter(this);
        StatisticDetailModel model = new StatisticDetailModel(presenter);

        presenter.setModel(model);
        mPresenter = presenter;

        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getApplicationContext());

        Bundle count = new Bundle();
        count.putString(FS.EXTRA_STATISTIC, FS.STATISTIC_COUNT);
        creator.add(R.string.statistic_count, StatisticCountFragment.class, count);

        Bundle income = new Bundle();
        income.putString(FS.EXTRA_STATISTIC, FS.STATISTIC_COUNT);
        creator.add(R.string.statistic_income, StatisticIncomeFragment.class, income);

        mAdapter = new FragmentPagerItemAdapter(getSupportFragmentManager(), creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    @Override
    public AppCompatActivity getActivity() {
        return StatisticDetailActivity.this;
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showWaiting() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.dialog_message_waiting), "", true);
            }
        });
    }

    @Override
    public void hideWaiting() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        });

    }

    @Override
    public void requestPresent() {
        StatisticCountFragment page1 = (StatisticCountFragment) mAdapter.getPage(0);
        page1.onResume();

        StatisticIncomeFragment page2 = (StatisticIncomeFragment) mAdapter.getPage(1);
        page2.onResume();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
