package product.dg.xshipper.manage_gig.completed.Error;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.util.FS;

public class ErrorFragment extends Fragment implements ErrorInterfaces.RequiredViewOps {

    private ErrorInterfaces.ProvidedPresenterOps mPresenter;

    ProgressDialog mProgressDialog;

    ListView mGigListView;
    TextView mNoGig;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_gig_fragment, container, false);

        bindingControls(view);
        setupControlEvents();

        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    private void bindingControls(View view) {
        mGigListView = (ListView) view.findViewById(R.id.lv_gig_manage);
        mNoGig = (TextView) view.findViewById(R.id.no_ticket);
    }

    private void setupControlEvents() {
        mGigListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showOption(i);
                //mPresenter.goToDetail(i);
            }
        });
    }

    void showOption(final int i) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_complete_gig_item, null);
        dialogBuilder.setView(dialogView);

        Button viewProfile = (Button) dialogView.findViewById(R.id.btn_view_profile);
        Button rating = (Button) dialogView.findViewById(R.id.btn_rating);

        if (mPresenter.isShowRating(i)) {
            rating.setVisibility(View.VISIBLE);
        }
        else {
            rating.setVisibility(View.GONE);
        }

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.requestViewCustomerProfile(i);
                alertDialog.dismiss();
            }
        });

        rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRating(i);
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    void showRating(final int index) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_rating, null);
        dialogBuilder.setView(dialogView);

        final RatingBar ratingBar = (RatingBar) dialogView.findViewById(R.id.rating);
        Button submit = (Button) dialogView.findViewById(R.id.btn_submit);

        final android.app.AlertDialog alertDialog = dialogBuilder.create();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rate = (int) ratingBar.getRating();
                mPresenter.rating(index, Integer.toString(rate));
                alertDialog.dismiss();
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    private void init() {
        ErrorPresenter presenter = new ErrorPresenter(this);
        ErrorModel model = new ErrorModel(presenter);

        presenter.setModel(model);

        mPresenter = presenter;
    }

    @Override
    public AppCompatActivity getParentActivity() {
        return (AppCompatActivity) ErrorFragment.this.getActivity();
    }

    @Override
    public void showWaiting() {
        mProgressDialog = ProgressDialog.show(getParentActivity(), getString(R.string.dialog_message_waiting), "", true);
    }

    @Override
    public void hideWaiting() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void showDialog(int title, int message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(getString(message))
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void showDialog(int title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(getParentActivity())
                .setTitle(getString(title))
                .setMessage(message)
                .setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        if (mGigListView != null) {
            mGigListView.setAdapter(adapter);
        }
    }

    @Override
    public void onNoTicket() {
        mNoGig.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHaveTicket() {
        mNoGig.setVisibility(View.GONE);
    }

    @Override
    public void ratingCompleted() {
        mPresenter.loadData();
        showDialog(R.string.rating_title, R.string.rating_success);
    }

    @Override
    public void presentCustomer(Customer data) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(getParentActivity());
        LayoutInflater inflater = getParentActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_user_info, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        ImageView avatar = (ImageView) dialogView.findViewById(R.id.iv_avatar);
        TextView name = (TextView) dialogView.findViewById(R.id.tv_name);
        RatingBar rating = (RatingBar) dialogView.findViewById(R.id.rb_rating);
        TextView phone = (TextView) dialogView.findViewById(R.id.tv_phone);
        TextView mail = (TextView) dialogView.findViewById(R.id.tv_email);
        TextView count = (TextView) dialogView.findViewById(R.id.tv_ticket_count);

        name.setText(data.getName());
        phone.setText(data.getPhone());
        mail.setText(data.getEmail());
        count.setText(data.getCount());
        rating.setRating(Integer.parseInt(data.getRating()));

        Picasso.with(getParentActivity()).load(FS.IMAGE_HOST + data.getAvatar()).into(avatar);

        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    @Override
    public void presentCustomerFail() {
        showDialog(R.string.dialog_title_error, R.string.gig_ticket_view_customer_fail);
    }
}
