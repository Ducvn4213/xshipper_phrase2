package product.dg.xshipper.manage_gig.completed.Error;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;

public class ErrorInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showWaiting();
        void hideWaiting();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);

        void onNoTicket();
        void onHaveTicket();

        void ratingCompleted();

        void presentCustomer(Customer data);
        void presentCustomerFail();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);

        void rating(int index, String rate);
        void requestViewCustomerProfile(int id);

        boolean isShowRating(int id);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);
        void updateNewData(List<GigTicket> data);
        GigTicketAdapter createAdapterWithData(List<GigTicket> data);

        void onNoTicket();
        void onHaveTicket();

        void ratingCompleted();

        void loadCustomerProfileSuccess(Customer data);
        void loadCustomerProfileFail();
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        void updateNewData(List<GigTicket> data);

        void rating(int index, String rate);

        void loadCustomerProfile(int id);

        boolean isShowRating(int id);
    }
}
