package product.dg.xshipper.manage_gig.completed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipper.R;
import product.dg.xshipper.manage_gig.completed.Error.ErrorFragment;
import product.dg.xshipper.manage_gig.completed.done.DoneFragment;
import product.dg.xshipper.manage_gig.completed.done_cod.DoneCODFragment;
import product.dg.xshipper.manage_gig.pick.pick_confirm.PickConfirmFragment;
import product.dg.xshipper.manage_gig.pick.waiting_pick.WaitingPickFragment;
import product.dg.xshipper.util.FS;

public class CompletedFragment extends Fragment implements ViewPager.OnPageChangeListener {

    SmartTabLayout mTab;
    ViewPager mViewPager;
    FragmentPagerItemAdapter mAdapter;

    private static CompletedFragment instance;
    public static CompletedFragment getInstance() {
        return instance;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_ticket, container, false);

        bindingUI(view);
        setupUIData();
        //setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        instance = this;
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle done = new Bundle();
        done.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_DONE);
        creator.add(R.string.manage_gigs_done, DoneFragment.class, done);

        Bundle doneCOD = new Bundle();
        doneCOD.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_DONE_COD);
        creator.add(R.string.manage_gigs_done_cod, DoneCODFragment.class, doneCOD);

        Bundle error = new Bundle();
        error.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_ERROR);
        creator.add(R.string.manage_gigs_error, ErrorFragment.class, error);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    public void setTitleForTabAt(int pos, int title, int nums) {
        TextView tv = (TextView) mTab.getTabAt(pos).findViewById(R.id.tv_ticket_tab_item);
        tv.setText(getString(title) + " [" + nums + "]");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
