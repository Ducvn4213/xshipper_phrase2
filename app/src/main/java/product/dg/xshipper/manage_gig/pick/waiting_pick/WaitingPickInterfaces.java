package product.dg.xshipper.manage_gig.pick.waiting_pick;

import android.support.v7.app.AppCompatActivity;

import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;

public class WaitingPickInterfaces {
    interface RequiredViewOps {
        AppCompatActivity getParentActivity();

        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void showWaiting();
        void hideWaiting();

        void updateAdapter(GigTicketAdapter adapter);

        void performReload();

        void onNoTicket();
        void onHaveTicket();

        void onConfirmOK();
        void onConfirmFail();

        void onCancelOK();
        void onCancelFail();

        void presentCustomer(Customer data);
        void presentCustomerFail();
    }

    interface ProvidedPresenterOps {
        void loadData();
        void goToDetail(int position);
        GigTicket getDataFromPosition(int pos);

        void confirm(int id);
        void cancel(int id);
        void requestViewCustomerProfile(int id);
    }

    interface RequiredPresenterOps {
        void showDialog(int title, int message);
        void showDialog(int title, String message);

        void updateAdapter(GigTicketAdapter adapter);
        void updateNewData(List<GigTicket> data);
        GigTicketAdapter createAdapterWithData(List<GigTicket> data);

        void onNoTicket();
        void onHaveTicket();

        void onConfirmOK();
        void onConfirmFail();

        void onCancelOK();
        void onCancelFail();

        void loadCustomerProfileSuccess(Customer data);
        void loadCustomerProfileFail();
    }

    interface ProvidedModelOps {
        void downloadData();
        int getIdFromPosition(int position);
        void updateNewData(List<GigTicket> data);
        GigTicket getDataFromPosition(int pos);

        void confirm(int id);
        void cancel(int id);
        void loadCustomerProfile(int id);
    }
}
