package product.dg.xshipper.manage_gig.completed.done;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;

public class DonePresenter implements DoneInterfaces.ProvidedPresenterOps, DoneInterfaces.RequiredPresenterOps {
    private WeakReference<DoneInterfaces.RequiredViewOps> mView;
    private DoneInterfaces.ProvidedModelOps mModel;

    DonePresenter(DoneInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(DoneInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void rating(int index, String rate) {
        mView.get().showWaiting();
        mModel.rating(index, rate);
    }

    @Override
    public void requestViewCustomerProfile(int id) {
        mView.get().showWaiting();
        mModel.loadCustomerProfile(id);
    }

    @Override
    public boolean isShowRating(int id) {
        return mModel.isShowRating(id);
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigTicket> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigTicketAdapter createAdapterWithData(List<GigTicket> data) {
        GigTicketAdapter adapter = new GigTicketAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void onNoTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onNoTicket();
            }
        });
    }

    @Override
    public void onHaveTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onHaveTicket();
            }
        });
    }

    @Override
    public void ratingCompleted() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().ratingCompleted();
            }
        });
    }

    @Override
    public void loadCustomerProfileSuccess(final Customer data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomer(data);

            }
        });
    }

    @Override
    public void loadCustomerProfileFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomerFail();
            }
        });
    }
}
