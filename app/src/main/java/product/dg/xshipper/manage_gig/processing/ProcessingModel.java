package product.dg.xshipper.manage_gig.processing;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.ManageGigFragment;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;
import product.dg.xshipper.util.Utils;

public class ProcessingModel implements ProcessingInterfaces.ProvidedModelOps {
    private ProcessingInterfaces.RequiredPresenterOps mPresenter;
    private GigTicketAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    ProcessingModel(ProcessingInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigTicket> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "3"));

        mService.get_book_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mPresenter.updateNewData(data.getData());
                mPresenter.onHaveTicket();
            }

            @Override
            public void onFail(String error) {
                List<GigTicket> data = new ArrayList<>();
                mPresenter.updateNewData(data);
                mPresenter.onNoTicket();
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public void updateNewData(List<GigTicket> data) {
        //ManageGigFragment.getInstance().setTitleForTabAt(2, R.string.manage_gigs_processing, data.size());
        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public GigTicket getDataFromPosition(int pos) {
        return mAdapter.getItem(pos);
    }

    @Override
    public void submitError(int id, String note) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", id + ""));
        paramList.add(new Param("_note", note));

        mService.endTicketError(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.onSubmitErrorSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.onSubmitErrorFail();
            }
        });
    }

    @Override
    public void submitSuccess(final int id, final String note, Bitmap image) {
        Utils.ConvertBitmapToStringTask convertBitmapToStringTask = new Utils.ConvertBitmapToStringTask();
        convertBitmapToStringTask.setBitmap(image);
        convertBitmapToStringTask.setCallback(new Utils.ConvertBitmapToStringCallback() {
            @Override
            public void onSuccess(final String string) {
                performSubmitSuccess(id, note, string);
            }

            @Override
            public void onFail() {
                mPresenter.onSubmitSuccessFail();
            }
        });
        convertBitmapToStringTask.execute();
    }

    @Override
    public void loadCustomerProfile(int id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", "" + id));

        mService.getCustomer(paramList, new FSService.Callback<Customer>() {
            @Override
            public void onSuccess(Customer data) {
                mPresenter.loadCustomerProfileSuccess(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.loadCustomerProfileFail();
            }
        });
    }

    void performSubmitSuccess(int id, String note, String data) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", id + ""));
        paramList.add(new Param("_note", note));
        paramList.add(new Param("_image_confirm", data));

        mService.endTicketSuccess(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.onSubmitSuccessSuccess();
            }

            @Override
            public void onFail(String error) {
                mPresenter.onSubmitSuccessFail();
            }
        });
    }
}
