package product.dg.xshipper.manage_gig.completed.done_cod;

import java.util.ArrayList;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.manage_gig.completed.CompletedFragment;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.response.GigTicketResponse;
import product.dg.xshipper.service.FSService;
import product.dg.xshipper.service.network.Param;

public class DoneCODModel implements DoneCODInterfaces.ProvidedModelOps {
    private DoneCODInterfaces.RequiredPresenterOps mPresenter;
    private GigTicketAdapter mAdapter;
    private FSService mService = FSService.getInstance();

    DoneCODModel(DoneCODInterfaces.RequiredPresenterOps presenter) {
        mPresenter = presenter;

        List<GigTicket> data = new ArrayList<>();
        mAdapter = mPresenter.createAdapterWithData(data);
        mPresenter.updateAdapter(mAdapter);
    }

    @Override
    public void downloadData() {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_status", "4"));

        mService.get_book_gig_ticket(paramList, new FSService.Callback<GigTicketResponse>() {
            @Override
            public void onSuccess(GigTicketResponse data) {
                mPresenter.updateNewData(data.getData());
                mPresenter.onHaveTicket();
            }

            @Override
            public void onFail(String error) {
                List<GigTicket> data = new ArrayList<>();
                mPresenter.updateNewData(data);
                mPresenter.onNoTicket();
            }
        });
    }

    @Override
    public int getIdFromPosition(int position) {
        return mAdapter.getItem(position).getId();
    }

    @Override
    public void updateNewData(List<GigTicket> data) {

        //CompletedFragment.getInstance().setTitleForTabAt(1, R.string.manage_gigs_done_cod, data.size());

        mAdapter.clear();
        mAdapter.addAll(data);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void rating(int index, String rate) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gigid", Integer.toString(mAdapter.getItem(index).getId())));
        paramList.add(new Param("_rate", rate));

        mService.rating(paramList, new FSService.Callback<String>() {
            @Override
            public void onSuccess(String data) {
                mPresenter.ratingCompleted();
            }

            @Override
            public void onFail(String error) {
                mPresenter.showDialog(R.string.dialog_title_error, error);
            }
        });
    }

    @Override
    public void loadCustomerProfile(int id) {
        List<Param> paramList = new ArrayList<>();
        paramList.add(new Param("_gig_id", "" + mAdapter.getItem(id).getId()));

        mService.getCustomer(paramList, new FSService.Callback<Customer>() {
            @Override
            public void onSuccess(Customer data) {
                mPresenter.loadCustomerProfileSuccess(data);
            }

            @Override
            public void onFail(String error) {
                mPresenter.loadCustomerProfileFail();
            }
        });
    }

    @Override
    public boolean isShowRating(int id) {
        if (mAdapter.getItem(id).getRate().equalsIgnoreCase("0")) {
            return true;
        }

        return false;
    }
}
