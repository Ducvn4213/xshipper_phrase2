package product.dg.xshipper.manage_gig.pick;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import product.dg.xshipper.R;
import product.dg.xshipper.manage_gig.pick.pick_confirm.PickConfirmFragment;
import product.dg.xshipper.manage_gig.pick.waiting_pick.WaitingPickFragment;
import product.dg.xshipper.util.FS;

public class PickFragment extends Fragment implements ViewPager.OnPageChangeListener {

    SmartTabLayout mTab;
    ViewPager mViewPager;
    FragmentPagerItemAdapter mAdapter;

    private static PickFragment instance;

    public static PickFragment getInstance() {
        return instance;
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_ticket, container, false);

        bindingUI(view);
        setupUIData();
        //setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        instance = this;
    }

    private void bindingUI(View view) {
        mTab = (SmartTabLayout) view.findViewById(R.id.stl_tab);
        mViewPager = (ViewPager) view.findViewById(R.id.vp_container);
    }

    private void setupUIData() {
        FragmentPagerItems.Creator creator = FragmentPagerItems.with(getContext());

        Bundle waiting_pick = new Bundle();
        waiting_pick.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_WAITING_PICK);
        creator.add(R.string.manage_gigs_waiting_pick, WaitingPickFragment.class, waiting_pick);

        Bundle pick_confirm = new Bundle();
        pick_confirm.putString(FS.EXTRA_MANAGE_GIG, FS.GIG_PICK_CONFIRM);
        creator.add(R.string.manage_gigs_pick_confirm, PickConfirmFragment.class, pick_confirm);

        mAdapter = new FragmentPagerItemAdapter(getChildFragmentManager(),
                creator.create());

        mViewPager.setAdapter(mAdapter);
        mTab.setCustomTabView(R.layout.layout_smart_tab, R.id.tv_ticket_tab_item);
        mTab.setViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
    }

    public void setTitleForTabAt(int pos, int title, int nums) {
        TextView tv = (TextView) mTab.getTabAt(pos).findViewById(R.id.tv_ticket_tab_item);
        tv.setText(getString(title) + " [" + nums + "]");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
