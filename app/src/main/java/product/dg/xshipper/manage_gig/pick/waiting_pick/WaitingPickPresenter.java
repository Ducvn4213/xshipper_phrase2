package product.dg.xshipper.manage_gig.pick.waiting_pick;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;

public class WaitingPickPresenter implements WaitingPickInterfaces.ProvidedPresenterOps, WaitingPickInterfaces.RequiredPresenterOps {
    private WeakReference<WaitingPickInterfaces.RequiredViewOps> mView;
    private WaitingPickInterfaces.ProvidedModelOps mModel;

    WaitingPickPresenter(WaitingPickInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(WaitingPickInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void goToDetail(int position) {
    }

    @Override
    public GigTicket getDataFromPosition(int pos) {
        return mModel.getDataFromPosition(pos);
    }

    @Override
    public void confirm(int id) {
        mModel.confirm(id);
    }

    @Override
    public void cancel(int id) {
        mModel.cancel(id);
    }

    @Override
    public void requestViewCustomerProfile(int id) {
        mView.get().showWaiting();
        mModel.loadCustomerProfile(id);
    }

    @Override
    public void onConfirmOK() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onConfirmOK();
                mView.get().performReload();
            }
        });
    }

    @Override
    public void onConfirmFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onConfirmFail();
                mView.get().performReload();
            }
        });
    }

    @Override
    public void onCancelOK() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onCancelOK();
                mView.get().performReload();
            }
        });
    }

    @Override
    public void onCancelFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onCancelFail();
                mView.get().performReload();
            }
        });
    }

    @Override
    public void loadCustomerProfileSuccess(final Customer data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomer(data);

            }
        });
    }

    @Override
    public void loadCustomerProfileFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomerFail();
            }
        });
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigTicket> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigTicketAdapter createAdapterWithData(List<GigTicket> data) {
        GigTicketAdapter adapter = new GigTicketAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void onNoTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onNoTicket();
            }
        });
    }

    @Override
    public void onHaveTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onHaveTicket();
            }
        });
    }
}
