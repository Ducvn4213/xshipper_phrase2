package product.dg.xshipper.manage_gig.processing;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;

import java.lang.ref.WeakReference;
import java.util.List;

import product.dg.xshipper.R;
import product.dg.xshipper.adapter.GigTicketAdapter;
import product.dg.xshipper.model.Customer;
import product.dg.xshipper.model.GigTicket;
import product.dg.xshipper.util.FS;

public class ProcessingPresenter implements ProcessingInterfaces.ProvidedPresenterOps, ProcessingInterfaces.RequiredPresenterOps {
    private WeakReference<ProcessingInterfaces.RequiredViewOps> mView;
    private ProcessingInterfaces.ProvidedModelOps mModel;

    ProcessingPresenter(ProcessingInterfaces.RequiredViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(ProcessingInterfaces.ProvidedModelOps model) {
        mModel = model;
    }

    @Override
    public void loadData() {
        mModel.downloadData();
    }

    @Override
    public void showDialog(final int title,final int message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void showDialog(final int title,final String message) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().showDialog(title, message);
            }
        });
    }

    @Override
    public void goToDetail(int position) {
    }

    @Override
    public void getImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        ActivityCompat.startActivityForResult(mView.get().getParentActivity(), Intent.createChooser(cameraIntent, "Select Picture"), FS.PICK_IMAGE_COMFIRM_REQUEST_CODE, null);
    }

    @Override
    public void error(final GigTicket data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().performError(data);
            }
        });
    }

    @Override
    public void success(final GigTicket data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().performSuccess(data);
            }
        });
    }

    @Override
    public void submitError(int id, String note) {
        if (note.trim().equalsIgnoreCase("")) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_error_note);
            return;
        }

        mView.get().showWaiting();
        mModel.submitError(id, note);
    }

    @Override
    public void submitSuccess(int id, String note, Bitmap image) {
        if (image == null) {
            showDialog(R.string.dialog_title_error, R.string.dialog_message_missing_image_confirm);
            return;
        }

        mView.get().showWaiting();
        mModel.submitSuccess(id, note, image);
    }

    @Override
    public void end(final GigTicket data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().performEndOption(data);
            }
        });
    }

    @Override
    public GigTicket getDataFromPosition(int pos) {
        return mModel.getDataFromPosition(pos);
    }

    @Override
    public void requestViewCustomerProfile(int id) {
        mView.get().showWaiting();
        mModel.loadCustomerProfile(id);
    }

    @Override
    public void updateAdapter(GigTicketAdapter adapter) {
        mView.get().updateAdapter(adapter);
    }

    @Override
    public void updateNewData(final List<GigTicket> data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mModel.updateNewData(data);
            }
        });
    }

    @Override
    public GigTicketAdapter createAdapterWithData(List<GigTicket> data) {
        GigTicketAdapter adapter = new GigTicketAdapter(mView.get().getParentActivity(), data);
        return adapter;
    }

    @Override
    public void onNoTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onNoTicket();
            }
        });
    }

    @Override
    public void onHaveTicket() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().onHaveTicket();
            }
        });
    }

    @Override
    public void onSubmitErrorSuccess() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().onSubmitErrorSuccess();
            }
        });
    }

    @Override
    public void onSubmitErrorFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().onSubmitErrorFail();
            }
        });
    }

    @Override
    public void onSubmitSuccessSuccess() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().onSubmitSuccessSuccess();
            }
        });
    }

    @Override
    public void onSubmitSuccessFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().onSubmitSuccessFail();
            }
        });
    }

    @Override
    public void loadCustomerProfileSuccess(final Customer data) {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomer(data);

            }
        });
    }

    @Override
    public void loadCustomerProfileFail() {
        mView.get().getParentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mView.get().hideWaiting();
                mView.get().presentCustomerFail();
            }
        });
    }
}
